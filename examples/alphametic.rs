use chinstrap as csp;

fn main() {
    let mut model = csp::DefaultModel::new();

    let xs = model.int_var().range(0..=9).create_array(8);
    model.all_diff(&xs).post();
    let s = xs[0];
    let e = xs[1];
    let n = xs[2];
    let d = xs[3];
    let m = xs[4];
    let o = xs[5];
    let r = xs[6];
    let y = xs[7];
    model.ne(s, 0).post();
    model.ne(m, 0).post();

    let send = 1000 * s + 100 * e + 10 * n + d;
    let more = 1000 * m + 100 * o + 10 * r + e;
    let money = 10000 * m + 1000 * o + 100 * n + 10 * e + y;

    model.arith(send + more, money).post();

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    for soln in model.solve(search) {
        let s = soln.val(s);
        let e = soln.val(e);
        let n = soln.val(n);
        let d = soln.val(d);
        let m = soln.val(m);
        let o = soln.val(o);
        let r = soln.val(r);
        let y = soln.val(y);
        println!("  {}{}{}{}", 'S', 'E', 'N', 'D');
        println!("+ {}{}{}{}", 'M', 'O', 'R', 'E');
        println!("------");
        println!(" {}{}{}{}{}", 'M', 'O', 'N', 'E', 'Y');
        println!();

        println!("  {}{}{}{}", s, e, n, d);
        println!("+ {}{}{}{}", m, o, r, e);
        println!("------");
        println!(" {}{}{}{}{}", m, o, n, e, y);
    }
}
