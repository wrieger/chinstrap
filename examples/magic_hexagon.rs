use chinstrap as csp;

use itertools::Itertools;

fn main() {
    let (model, vars) = make_model();

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&vars)
        .choose_var_by(csp::VarStrategy::FailFirst)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    for soln in model.solve(search) {
        print_soln(&soln, &vars);
    }
}

fn make_model() -> (csp::DefaultModel, csp::model::Array<csp::DefaultIntVar>) {
    let mut model = csp::DefaultModel::new();

    let n: usize = 19;
    let sum = 38;

    let vars = model.int_var().range(1..=(n as i32)).create_array(n);
    model.all_diff(&vars).post();

    let map_char = |c: char| vars[(c as usize) - ('a' as usize)];
    let map_str = |s: &str| -> Vec<csp::DefaultIntVar> { s.chars().map(map_char).collect() };

    // -
    model.sum_eq(map_str("abc"), sum).post();
    model.sum_eq(map_str("defg"), sum).post();
    model.sum_eq(map_str("hijkl"), sum).post();
    model.sum_eq(map_str("mnop"), sum).post();
    model.sum_eq(map_str("qrs"), sum).post();

    // /
    model.sum_eq(map_str("adh"), sum).post();
    model.sum_eq(map_str("beim"), sum).post();
    model.sum_eq(map_str("cfjnq"), sum).post();
    model.sum_eq(map_str("gkor"), sum).post();
    model.sum_eq(map_str("lps"), sum).post();

    // \
    model.sum_eq(map_str("cgl"), sum).post();
    model.sum_eq(map_str("bfkp"), sum).post();
    model.sum_eq(map_str("aejos"), sum).post();
    model.sum_eq(map_str("dinr"), sum).post();
    model.sum_eq(map_str("hmq"), sum).post();

    // symmetry breaking
    let a = map_char('a');
    let c = map_char('c');
    let h = map_char('h');
    let l = map_char('l');
    let q = map_char('q');
    let s = map_char('s');
    model.lt(a, c).post();
    model.lt(a, h).post();
    model.lt(a, l).post();
    model.lt(a, q).post();
    model.lt(a, s).post();
    model.lt(c, h).post();

    (model, vars)
}

fn print_soln(soln: &csp::DefaultSolution, vars: &csp::model::Array<csp::DefaultIntVar>) {
    let make_row = |i, len| {
        (i..(i + len))
            .map(|i| format!("{:2}", soln.val(vars[i])))
            .intersperse("  ".to_owned())
            .collect::<String>()
    };
    println!("    {}\n", make_row(0, 3));
    println!("  {}\n", make_row(3, 4));
    println!("{}\n", make_row(7, 5));
    println!("  {}\n", make_row(12, 4));
    println!("    {}\n", make_row(16, 3));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn magic_hexagon() {
        let (model, vars) = make_model();

        let search = csp::DefaultSearchBuilder::new()
            .branch_on(&vars)
            .choose_var_by(csp::VarStrategy::FailFirst)
            .choose_val_by(csp::ValStrategy::Minimum)
            .finish();

        let solns = model.solve(search).collect::<Vec<_>>();
        assert_eq!(solns.len(), 1);
    }
}
