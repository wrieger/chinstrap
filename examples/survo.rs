use chinstrap as csp;

use itertools::Itertools;

use chinstrap::model::Matrix;

struct Survo {
    n_cols: usize,
    n_rows: usize,
    row_sums: Vec<i32>,
    col_sums: Vec<i32>,
    board: Vec<((usize, usize), i32)>,
}

fn main() {
    let survo = Survo {
        n_rows: 4,
        n_cols: 5,
        row_sums: vec![68, 16, 86, 40],
        col_sums: vec![43, 37, 29, 54, 47],
        board: vec![],
    };

    println!("unsolved:");
    print_survo(&survo, None);

    let (model, xs) = make_model(&survo);

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::FailFirst)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    for soln in model.solve(search) {
        println!("solved:");
        print_survo(&survo, Some((&soln, &xs)));
    }
}

fn make_model(survo: &Survo) -> (csp::DefaultModel, Matrix<csp::DefaultIntVar>) {
    let mut model = csp::DefaultModel::new();

    let xs = model
        .int_var()
        .range(1..=((survo.n_rows * survo.n_cols) as i32))
        .create_matrix(survo.n_rows, survo.n_cols);

    model.all_diff(&xs).post();

    for r in 0..survo.n_rows {
        model.sum_eq(xs.row(r), survo.row_sums[r]).post();
    }

    for c in 0..survo.n_cols {
        model.sum_eq(xs.col(c), survo.col_sums[c]).post();
    }

    for &((r, c), val) in &survo.board {
        model.eq(xs[(r, c)], val).post();
    }

    (model, xs)
}

fn print_survo(
    survo: &Survo,
    soln_data: Option<(&csp::DefaultSolution, &Matrix<csp::DefaultIntVar>)>,
) {
    // largest number appearing in the puzzle (for alignment purposes)
    let max_sum = survo
        .row_sums
        .iter()
        .chain(survo.col_sums.iter())
        .max()
        .cloned()
        .unwrap();
    let n_digits = ((max_sum as f32).log(10.).floor() + 1.) as usize;
    let border_row = format!(
        "+{:->row_width$}+",
        "",
        row_width = survo.n_cols * (n_digits + 1) + 1
    );

    // top border
    println!("{}", border_row);

    for r in 0..survo.n_rows {
        // board row
        println!(
            "| {} | {}",
            (0..survo.n_cols)
                .map(|c| if let Some((soln, xs)) = soln_data {
                    // if a solution was provided, use it
                    format!("{:width$}", soln.val(xs[(r, c)]), width = n_digits)
                } else if let Some((_, val)) = survo
                    .board
                    .iter()
                    .find(|&&((r0, c0), _)| r == r0 && c == c0)
                {
                    // else if the initial board was provided, use it
                    format!("{:width$}", val, width = n_digits)
                } else {
                    // use a blank of the correct width otherwise
                    format!("{:width$}", "", width = n_digits)
                }).intersperse(" ".to_owned())
                .collect::<String>(),
            survo.row_sums[r]
        );
    }

    // bottom border
    println!("{}", border_row);

    // column sums
    println!(
        "  {}",
        (0..survo.n_cols)
            .map(|c| format!("{:width$}", survo.col_sums[c], width = n_digits))
            .intersperse(" ".to_owned())
            .collect::<String>()
    );

    println!();
}

#[cfg(test)]
mod tests {
    use super::*;

    // Survo puzzle 34/2007 (100)
    // https://www.survo.fi/puzzles/
    #[test]
    fn survo_34_2007() {
        let survo = Survo {
            n_rows: 4,
            n_cols: 4,
            row_sums: vec![16, 29, 41, 50],
            col_sums: vec![15, 32, 38, 51],
            board: vec![((0, 0), 1), ((0, 3), 8), ((3, 0), 7), ((3, 3), 16)],
        };

        let expected = vec![
            vec![1, 3, 4, 8],
            vec![2, 6, 9, 12],
            vec![5, 10, 11, 15],
            vec![7, 13, 14, 16],
        ];

        test_survo(&survo, &expected);
    }

    // Survo puzzle 139/2008 (800) #443-10002
    // https://www.survo.fi/puzzles/
    #[test]
    fn survo_139_2008() {
        let survo = Survo {
            n_rows: 4,
            n_cols: 4,
            row_sums: vec![38, 29, 46, 23],
            col_sums: vec![55, 42, 12, 27],
            board: vec![],
        };

        let expected = vec![
            vec![15, 12, 3, 8],
            vec![13, 9, 2, 5],
            vec![16, 14, 6, 10],
            vec![11, 7, 1, 4],
        ];

        test_survo(&survo, &expected);
    }

    // Survo puzzle 374/2015 (1550) #363-25382
    // https://www.survo.fi/puzzles/
    #[test]
    fn survo_374_2015() {
        let survo = Survo {
            n_rows: 3,
            n_cols: 6,
            row_sums: vec![32, 50, 89],
            col_sums: vec![22, 13, 43, 30, 37, 26],
            board: vec![],
        };

        let expected = vec![
            vec![3, 1, 11, 5, 8, 4],
            vec![6, 2, 14, 9, 12, 7],
            vec![13, 10, 18, 16, 17, 15],
        ];

        test_survo(&survo, &expected);
    }

    // Survo puzzle 41/2007 (?)
    // https://www.survo.fi/puzzles/
    #[test]
    fn survo_41_2007() {
        let survo = Survo {
            n_rows: 3,
            n_cols: 3,
            row_sums: vec![12, 17, 16],
            col_sums: vec![14, 15, 16],
            board: vec![],
        };

        count_survo(&survo, 26);
    }

    // Survo puzzle 227/2011 (?) #444-44444
    // https://www.survo.fi/puzzles/
    #[test]
    fn survo_227_2011() {
        let survo = Survo {
            n_rows: 4,
            n_cols: 4,
            row_sums: vec![17, 54, 40, 25],
            col_sums: vec![49, 31, 38, 18],
            board: vec![],
        };

        count_survo(&survo, 3);
    }

    fn test_survo(survo: &Survo, expected: &Vec<Vec<i32>>) {
        let (model, xs) = make_model(survo);

        let search = csp::DefaultSearchBuilder::new()
            .branch_on(&xs)
            .choose_var_by(csp::VarStrategy::FailFirst)
            .choose_val_by(csp::ValStrategy::Minimum)
            .finish();

        let solns = model.solve(search).collect::<Vec<_>>();
        assert_eq!(solns.len(), 1);
        let soln = &solns[0];
        for r in 0..survo.n_rows {
            for c in 0..survo.n_cols {
                assert_eq!(soln.val(xs[(r, c)]), expected[r][c]);
            }
        }
    }

    fn count_survo(survo: &Survo, n_solns: usize) {
        let (model, xs) = make_model(survo);

        let search = csp::DefaultSearchBuilder::new()
            .branch_on(&xs)
            .choose_var_by(csp::VarStrategy::FailFirst)
            .choose_val_by(csp::ValStrategy::Minimum)
            .finish();

        assert_eq!(model.solve(search).count(), n_solns);
    }
}
