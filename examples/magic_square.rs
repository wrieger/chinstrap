use chinstrap as csp;

fn main() {
    let n: usize = std::env::args()
        .nth(1)
        .and_then(|n_str| n_str.parse().ok())
        .unwrap_or(3);
    assert!(n >= 1);

    let magic_sum = (n * (n * n + 1) / 2) as i32;

    let mut model = csp::DefaultModel::new();

    let xs = model
        .int_var()
        .range(1..=(n * n) as i32)
        .create_matrix(n, n);

    model.all_diff(&xs).post();

    for r in 0..n {
        model.sum_eq(xs.row(r), magic_sum).post();
    }

    for c in 0..n {
        model.sum_eq(xs.col(c), magic_sum).post();
    }

    let diag0 = (0..n).map(|r| xs.row(r).nth(r).unwrap());
    model.sum_eq(diag0, magic_sum).post();

    let diag1 = (0..n).map(|r| xs.row(n - r - 1).nth(r).unwrap());
    model.sum_eq(diag1, magic_sum).post();

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    let max_digits = (((n * n) as f32).log(10f32).floor() + 1.) as usize;
    let mut count = 0;
    for soln in model.solve(search) {
        count += 1;
        for r in 0..n {
            for c in 0..n {
                print!("{:ident$} ", soln.val(xs[(r, c)]), ident = max_digits);
            }
            println!();
        }
        println!();
    }
    println!("{} solutions found", count);
}
