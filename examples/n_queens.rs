use chinstrap as csp;

extern crate itertools;
extern crate structopt;

use itertools::Itertools;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    /// Counts the number of solutions
    #[structopt(short = "c", long = "--count")]
    count: bool,

    /// The size of the board
    #[structopt(name = "N", default_value = "8")]
    n: usize,
}

fn main() {
    let opt = Opt::from_args();

    let (model, xs) = make_model(opt.n);

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::FailFirst)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    // solve the model
    if opt.count {
        let count = model.solve(search).count();
        println!("{} queens: {} solutions found", opt.n, count);
    } else {
        if let Some(soln) = model.solve(search).next() {
            print_solution(opt.n, &soln, &xs);
        }
    }
}

fn make_model(n: usize) -> (csp::DefaultModel, csp::model::Array<csp::DefaultIntVar>) {
    // create a new model
    let mut model = csp::DefaultModel::new();

    // create an array of n integer variables, each with the domain 0..n
    let xs = model.int_var().range(0..(n as i32)).create_array(n);

    // post constraint x_i != x_j
    model.all_diff(&xs).post();

    // post constraint x_i + i != x_j + j
    model
        .all_diff(xs.iter().enumerate().map(|(i, x)| x.offset(i as i32)))
        .post();

    // post constraint x_i - i != x_j - j
    model
        .all_diff(xs.iter().enumerate().map(|(i, x)| x.offset(-(i as i32))))
        .post();

    (model, xs)
}

fn print_solution(
    n: usize,
    soln: &csp::DefaultSolution,
    vars: &csp::model::Array<csp::DefaultIntVar>,
) {
    let border_row = format!("+{:->width$}+", "", width = 2 * n + 1);
    println!("{}", border_row);
    for r in 0..n {
        println!(
            "| {} |",
            (0..n)
                .map(|c| if soln.val(vars[r]) == c as i32 {
                    'Q'
                } else {
                    '.'
                }).intersperse(' ')
                .collect::<String>()
        );
    }
    println!("{}", border_row);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn n_queens_count() {
        let known = [
            (1, 1),
            (2, 0),
            (3, 0),
            (4, 2),
            (5, 10),
            (6, 4),
            (7, 40),
            (8, 92),
        ];

        for &(n, count) in &known {
            let (model, xs) = make_model(n);

            let search = csp::DefaultSearchBuilder::new()
                .branch_on(&xs)
                .choose_var_by(csp::VarStrategy::FailFirst)
                .choose_val_by(csp::ValStrategy::Minimum)
                .finish();

            assert_eq!(model.solve(search).count(), count);
        }
    }
}
