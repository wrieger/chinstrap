#![feature(duration_as_u128)]

use chinstrap as csp;

// initial sudoku board, 0 represents a blank
// const BOARD: [[i32; 9]; 9] = [
//     [8, 0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 3, 6, 0, 0, 0, 0, 0],
//     [0, 7, 0, 0, 9, 0, 2, 0, 0],
//     [0, 5, 0, 0, 0, 7, 0, 0, 0],
//     [0, 0, 0, 0, 4, 5, 7, 0, 0],
//     [0, 0, 0, 1, 0, 0, 0, 3, 0],
//     [0, 0, 1, 0, 0, 0, 0, 6, 8],
//     [0, 0, 8, 5, 0, 0, 0, 1, 0],
//     [0, 9, 0, 0, 0, 0, 4, 0, 0],
// ];
const BOARD: [[i32; 9]; 9] = [
    [6, 0, 0, 0, 0, 8, 9, 4, 0],
    [9, 0, 0, 0, 0, 6, 1, 0, 0],
    [0, 7, 0, 0, 4, 0, 0, 0, 0],
    [2, 0, 0, 6, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 2, 0, 0],
    [0, 8, 9, 0, 0, 2, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 0, 0, 5],
    [0, 0, 0, 0, 0, 0, 0, 3, 0],
    [8, 0, 0, 0, 0, 1, 6, 0, 0],
];

fn main() {
    // create a new model
    let mut model = csp::DefaultModel::new();

    // create a 9 x 9 matrix of integer variables, each with domain { 1, 2, ..., 9 }
    let xs = model.int_var().range(1..=9).create_matrix(9, 9);

    // each row has distinct values
    for r in 0..9 {
        model.all_diff(xs.row(r)).post();
    }

    // each column has distinct values
    for c in 0..9 {
        model.all_diff(xs.col(c)).post();
    }

    // each block has distinct values
    for r in (0..3).map(|r| r * 3) {
        for c in (0..3).map(|c| c * 3) {
            model.all_diff(xs.sub_matrix(r, c, 3, 3)).post();
        }
    }

    // set the initial conditions of the board
    for r in 0..9 {
        for c in 0..9 {
            if BOARD[r][c] != 0 {
                model.eq(xs[(r, c)], BOARD[r][c]).post();
            }
        }
    }

    // define the search
    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    // solve the model with the given search
    let soln = model.solve(search).next().unwrap();
    for r in 0..9 {
        for c in 0..9 {
            print!("{}", soln.val(xs[(r, c)]));
        }
        println!();
    }
}
