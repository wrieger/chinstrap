use chinstrap as csp;

fn main() {
    let n: usize = std::env::args()
        .nth(1)
        .map(|n_str| n_str.parse().expect("could not parse n"))
        .unwrap_or(8);
    assert!(n > 0, "n must be positive");

    let mut model = csp::DefaultModel::new();

    let xs = model.int_var().range(1..=(n as i32)).create_array(n);
    let deltas = model.int_var().range(1..(n as i32)).create_array(n - 1);

    model.all_diff(&xs).post();
    model.all_diff(&deltas).post();

    for i in 0..(n - 1) as usize {
        let d = model.int_var().expr(xs[i + 1] - xs[i]).create();
        model.abs(d, deltas[i]).post();
    }

    model.lt(xs[0], xs[n - 1]).post();
    model.lt(deltas[0], deltas[1]).post();

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&xs)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    // println!("{}", model.solve(csp::VarStrategy::InOrder, csp::ValStrategy::Minimum).next().is_some());
    let mut count = 0;
    for soln in model.solve(search).take(1) {
        for (&x, &delta) in xs.iter().zip(deltas.iter()) {
            print!("{} ({}) ", soln.val(x), soln.val(delta));
        }
        println!("{}", soln.val(xs[(n - 1) as usize]));
        count += 1;
    }
    println!("{}", count);
}
