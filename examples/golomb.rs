use chinstrap as csp;

fn main() {
    let n: usize = std::env::args()
        .nth(1)
        .and_then(|n_str| n_str.parse().ok())
        .unwrap_or(4);
    assert!(n >= 2);

    let mut model = csp::DefaultModel::new();
    let marks = model
        .int_var()
        .range(0..(2i32.pow(n as u32) - 1))
        .create_array(n);

    model.eq(marks[0], 0).post();
    for i in 1..n {
        model.le(marks[i - 1], marks[i]).post();
    }

    let mut differences = Vec::with_capacity(n * (n - 1) / 2);
    for i in 0..n {
        for j in (i + 1)..n {
            differences.push(model.int_var().expr(marks[j] - marks[i]).create());
        }
    }
    model.all_diff(&differences).post();

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&marks)
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    for soln in model.minimize(search, marks[n - 1]) {
        for i in 0..n {
            print!("{} ", soln.val(marks[i]));
        }
        println!();
    }
}
