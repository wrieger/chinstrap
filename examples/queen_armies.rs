use chinstrap as csp;
use itertools::Itertools;
use structopt::StructOpt;

use chinstrap::model::Matrix;

#[derive(StructOpt)]
struct Opt {
    /// The size of the board
    #[structopt(name = "N", default_value = "4")]
    n: usize,
}

fn main() {
    let opt = Opt::from_args();
    let n = opt.n;

    let (model, s, army_size) = model_smarter(n);
    // let (model, b, w, army_size) = model_naive(n);

    let search = csp::DefaultSearchBuilder::new()
        .branch_on(&s)
        // .branch_on(b.iter().chain(w.iter()))
        .choose_var_by(csp::VarStrategy::InOrder)
        .choose_val_by(csp::ValStrategy::Maximum)
        .finish();

    for soln in model.maximize(search, army_size) {
        println!(
            "solution found with armies of size {}:",
            soln.val(army_size)
        );
        print_smarter_solution(&soln, n, &s);
    }
}

fn model_smarter(
    n: usize,
) -> (
    csp::DefaultModel,
    Matrix<csp::DefaultIntVar>,
    csp::DefaultIntVar,
) {
    let mut model = csp::DefaultModel::new();
    let s = model.int_var().range(0..=2).create_matrix(n, n);
    let army_size = model.int_var().range(0..=(n * n) as i32).create();
    model.count(&s, 1, army_size).post();
    model.count(&s, 2, army_size).post();

    let rows = model.int_var().range(0..=1).create_array(n);
    let cols = model.int_var().range(0..=1).create_array(n);
    let diags0 = model.int_var().range(0..=1).create_array(2 * n - 1);
    let diags1 = model.int_var().range(0..=1).create_array(2 * n - 1);

    for r in 0..n {
        for c in 0..n {
            model.ne(s[(r, c)], rows[r].minus().offset(2)).post();
            model.ne(s[(r, c)], cols[c].minus().offset(2)).post();
            model.ne(s[(r, c)], diags0[r + c].minus().offset(2)).post();
            model
                .ne(s[(r, c)], diags1[(n - r - 1) + c].minus().offset(2))
                .post();
        }
    }

    (model, s, army_size)
}

fn model_naive(
    n: usize,
) -> (
    csp::DefaultModel,
    Matrix<csp::DefaultIntVar>,
    Matrix<csp::DefaultIntVar>,
    csp::DefaultIntVar,
) {
    let mut model = csp::DefaultModel::new();
    let b = model.int_var().range(0..=1).create_matrix(n, n);
    let w = model.int_var().range(0..=1).create_matrix(n, n);
    let b_tot = model.int_var().range(0..=(n * n) as i32).create();
    let w_tot = model.int_var().range(0..=(n * n) as i32).create();

    model.sum_eq(&b, b_tot).post();
    model.sum_eq(&w, w_tot).post();
    model.eq(b_tot, w_tot).post();

    for r0 in 0..n {
        for r1 in 0..n {
            for c0 in 0..n {
                for c1 in 0..n {
                    if (r0 == r1) || (c0 == c1) || (r1 + c0 == c1 + r0) || (r0 + c0 == r1 + c1) {
                        let x = b[(r0, c0)];
                        let y = w[(r1, c1)];
                        model.arith_lt(x + y, 2).post();
                    }
                }
            }
        }
    }

    (model, b, w, b_tot)
}

fn model_naive_check(
    dim: usize,
    n: i32,
) -> (
    csp::DefaultModel,
    Matrix<csp::DefaultIntVar>,
    Matrix<csp::DefaultIntVar>,
) {
    let mut model = csp::DefaultModel::new();
    let b = model.int_var().range(0..=1).create_matrix(dim, dim);
    let w = model.int_var().range(0..=1).create_matrix(dim, dim);
    model.count(&b, 1, n).post();
    model.count(&w, 1, n).post();

    for r0 in 0..dim {
        for r1 in 0..dim {
            for c0 in 0..dim {
                for c1 in 0..dim {
                    if (r0 == r1) || (c0 == c1) || (r1 + c0 == c1 + r0) || (r0 + c0 == r1 + c1) {
                        let x = b[(r0, c0)];
                        let y = w[(r1, c1)];
                        model.arith_lt(x + y, 2).post();
                    }
                }
            }
        }
    }

    (model, b, w)
}

fn make_checking_model(
    dim: i32,
    n: i32,
) -> (
    csp::DefaultModel,
    [csp::model::Array<csp::DefaultIntVar>; 4],
) {
    assert!(dim > 0);
    assert!(n > 0);

    let mut model = csp::DefaultModel::new();

    let b_cols = model.int_var().range(0..dim).create_array(n as usize);
    let b_rows = model.int_var().range(0..dim).create_array(n as usize);
    let w_cols = model.int_var().range(0..dim).create_array(n as usize);
    let w_rows = model.int_var().range(0..dim).create_array(n as usize);

    for i in 0..(n as usize - 1) {
        model
            .arith_lt(
                b_rows[i] * (dim as i32) + b_cols[i],
                b_rows[i + 1] * (dim as i32) + b_cols[i + 1],
            ).post();
        model
            .arith_lt(
                w_rows[i] * (dim as i32) + w_cols[i],
                w_rows[i + 1] * (dim as i32) + w_cols[i + 1],
            ).post();

        for b in 0..(n as usize) {
            for w in 0..(n as usize) {
                model.ne(b_rows[b], w_rows[w]).post();
                model.ne(b_cols[b], w_cols[w]).post();
                model
                    .arith_ne(b_rows[b] + b_cols[b], w_rows[w] + w_cols[w])
                    .post();
                model
                    .arith_ne(b_rows[b] + w_cols[w], w_rows[w] + b_cols[b])
                    .post();
            }
        }
    }

    (model, [b_rows, b_cols, w_rows, w_cols])
}

fn print_naive_solution(
    soln: &csp::DefaultSolution,
    n: usize,
    b: &Matrix<csp::DefaultIntVar>,
    w: &Matrix<csp::DefaultIntVar>,
) {
    let border_row = format!(
        "+{}+",
        std::iter::repeat("-").take(2 * n + 1).collect::<String>()
    );
    println!("{}", border_row);
    for r in 0..n {
        println!(
            "| {} |",
            (0..n)
                .map(|c| if soln.val(b[(r, c)]) == 1 {
                    "B"
                } else if soln.val(w[(r, c)]) == 1 {
                    "W"
                } else {
                    "."
                }).intersperse(" ")
                .collect::<String>()
        );
    }
    println!("{}", border_row);
}

fn print_smarter_solution(soln: &csp::DefaultSolution, n: usize, s: &Matrix<csp::DefaultIntVar>) {
    let border_row = format!(
        "+{}+",
        std::iter::repeat("-").take(2 * n + 1).collect::<String>()
    );
    println!("{}", border_row);
    for r in 0..n {
        println!(
            "| {} |",
            (0..n)
                .map(|c| if soln.val(s[(r, c)]) == 1 {
                    "B"
                } else if soln.val(s[(r, c)]) == 2 {
                    "W"
                } else {
                    "."
                }).intersperse(" ")
                .collect::<String>()
        );
    }
    println!("{}", border_row);
}
