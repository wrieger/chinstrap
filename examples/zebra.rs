use chinstrap as csp;

fn main() {
    let mut model = csp::DefaultModel::new();

    // houses are numbered { 1, 2, 3, 4, 5 }
    // each variable represents the house with that quality
    // e.g., the value of `green` is the number of the green house

    let colors = model.int_var().range(1..=5).create_array(5);
    let (red, green, ivory, yellow, blue) = (colors[0], colors[1], colors[2], colors[3], colors[4]);

    let people = model.int_var().range(1..=5).create_array(5);
    let (englishman, spaniard, ukranian, norwegian, japanese) =
        (people[0], people[1], people[2], people[3], people[4]);

    let pets = model.int_var().range(1..=5).create_array(5);
    let (dog, snail, fox, horse, zebra) = (pets[0], pets[1], pets[2], pets[3], pets[4]);

    let drinks = model.int_var().range(1..=5).create_array(5);
    let (coffee, tea, milk, orange_juice, water) =
        (drinks[0], drinks[1], drinks[2], drinks[3], drinks[4]);

    let smokes = model.int_var().range(1..=5).create_array(5);
    let (old_gold, kool, chesterfield, lucky_strike, parliament) =
        (smokes[0], smokes[1], smokes[2], smokes[3], smokes[4]);

    // no houses share a quality
    model.all_diff(&colors).post();
    model.all_diff(&people).post();
    model.all_diff(&pets).post();
    model.all_diff(&drinks).post();
    model.all_diff(&smokes).post();

    // 2. The Englishman lives in the red house.
    model.eq(englishman, red).post();

    // 3. The Spaniard owns the dog.
    model.eq(spaniard, dog).post();

    // 4. Coffee is drunk in the green house.
    model.eq(coffee, green).post();

    // 5. The Ukrainian drinks tea.
    model.eq(ukranian, tea).post();

    // 6. The green house is immediately to the right of the ivory house.
    model.arith(green, ivory + 1).post();

    // 7. The Old Gold smoker owns snails.
    model.eq(old_gold, snail).post();

    // 8. Kools are smoked in the yellow house.
    model.eq(kool, yellow).post();

    // 9. Milk is drunk in the middle house.
    model.eq(milk, 3).post();

    // 10. The Norwegian lives in the first house.
    model.eq(norwegian, 1).post();

    // 11. The man who smokes Chesterfields lives in the house next to the man with the fox.
    let d1 = model.int_var().expr(chesterfield - fox).create();
    model.abs(d1, 1).post();

    // 12. Kools are smoked in the house next to the house where the horse is kept.
    let d2 = model.int_var().expr(kool - horse).create();
    model.abs(d2, 1).post();

    // 13. The Lucky Strike smoker drinks orange juice.
    model.eq(lucky_strike, orange_juice).post();

    // 14. The Japanese smokes Parliaments.
    model.eq(japanese, parliament).post();

    // 15. The Norwegian lives next to the blue house.
    let d3 = model.int_var().expr(norwegian - blue).create();
    model.abs(d3, 1).post();

    let search = csp::DefaultSearchBuilder::new()
        .choose_var_by(csp::VarStrategy::FailFirst)
        .choose_val_by(csp::ValStrategy::Minimum)
        .finish();

    let soln = model.solve(search).next().unwrap();

    let string_name = |person| match person {
        person if person == englishman => "englishman",
        person if person == spaniard => "spaniard",
        person if person == ukranian => "ukranian",
        person if person == norwegian => "norwegian",
        person if person == japanese => "japanese",
        _ => unreachable!(),
    };

    // who drinks water?
    let water_drinker = people
        .iter()
        .find(|&&person| soln.val(person) == soln.val(water))
        .cloned()
        .unwrap();
    println!("the {} drinks water", string_name(water_drinker));
    assert_eq!(string_name(water_drinker), "norwegian");

    // who owns the zebra?
    let zebra_owner = people
        .iter()
        .find(|&&person| soln.val(person) == soln.val(zebra))
        .cloned()
        .unwrap();
    println!("the {} owns the zebra", string_name(zebra_owner));
    assert_eq!(string_name(zebra_owner), "japanese");
}
