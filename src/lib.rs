//! Chinstrap: a finite-domain constraint satisfaction library.

#[macro_use]
extern crate itertools;

pub mod linexpr;
pub mod model;
pub mod prop;
pub mod search;
pub mod types;
pub mod var;

pub type DefaultIntVar = var::Var<var::views::BaseView<var::domains::BitSetDom>>;
pub type DefaultModel = model::Model<var::domains::BitSetDom>;
pub type DefaultSearch = model::Search<var::domains::BitSetDom>;
pub type DefaultSearchBuilder = model::SearchBuilder<var::domains::BitSetDom>;
pub type DefaultSolution = search::Solution<var::domains::BitSetDom>;

pub use self::search::branch::{ValStrategy, VarStrategy};
