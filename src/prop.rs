pub mod all_diff;
pub mod boolean;
pub mod count;
pub mod math;
pub mod rel;
pub mod sum;

use crate::{
    types::Error,
    var::{self, ModEvent, ModTrigger, Var, View},
};

const NUM_COSTS: usize = 7;

/// Cost to propagate a propagator.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Cost {
    Unary,
    Binary,
    Ternary,
    Linear,
    Quadratic,
    Cubic,
    Slow,
}

/// Propagator status.
#[derive(Clone, Debug)]
pub enum Status<D>
where
    D: var::Domain,
{
    Entailed,
    Fixed,
    Unsure,
    Rewrite(Box<dyn Propagate<Dom = D>>),
}

impl<D> Status<D>
where
    D: var::Domain,
{
    pub fn is_entailed(&self) -> bool {
        match self {
            Status::Entailed => true,
            _ => false,
        }
    }
}

/// Propagator base trait.
pub trait Propagate: std::fmt::Debug {
    type Dom: var::Domain;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = Self::Dom>>;
    fn propagate(
        &mut self,
        ctx: &mut Context<'_, '_, Self::Dom>,
        first_time: bool,
    ) -> Result<Status<Self::Dom>, Error>;
    fn cost(&self) -> Cost;
}

impl<D> Clone for Box<dyn Propagate<Dom = D>>
where
    D: var::Domain,
{
    fn clone(&self) -> Self {
        self.boxed_clone()
    }
}

/// Context object used by propagators during propagation.
pub struct Context<'doms, 'events, D>
where
    D: var::Domain + 'doms,
{
    prop_token: Token,
    events: &'events mut Vec<(usize, var::ModEvent)>,
    doms: &'doms mut var::Store<D>,
}

impl<'doms, 'events, D> Context<'doms, 'events, D>
where
    D: var::Domain,
{
    pub fn new(
        prop_token: Token,
        doms: &'doms mut var::Store<D>,
        events: &'events mut Vec<(usize, ModEvent)>,
    ) -> Self {
        Self {
            prop_token,
            doms,
            events,
        }
    }

    pub fn subscribe<V>(&mut self, x: Var<V>, condition: ModTrigger)
    where
        V: View,
    {
        self.doms.subscribe(x, self.prop_token, condition);
    }

    pub fn unsubscribe<V, X>(&mut self, var: X)
    where
        V: View,
        X: Into<Var<V>>,
    {
        self.doms.unsubscribe(var, self.prop_token);
    }

    pub fn size<I, V>(&self, view: I) -> usize
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        self.doms.size(view)
    }

    pub fn is_empty<I, V>(&self, view: I) -> bool
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        self.doms.is_empty(view)
    }

    pub fn is_assigned<I, V>(&self, view: I) -> bool
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        self.doms.is_empty(view)
    }

    pub fn contains<V>(&self, x: Var<V>, val: V::Value) -> bool
    where
        V: View<Dom = D>,
    {
        self.doms.contains(x, val)
    }

    pub fn values<'a, 'v, V>(
        &'a self,
        view: &'v Var<V>,
    ) -> Box<dyn DoubleEndedIterator<Item = V::Value> + 'v>
    where
        V: View<Dom = D>,
        'a: 'v,
    {
        self.doms.values(view)
    }

    pub fn value<V>(&self, x: Var<V>) -> Option<V::Value>
    where
        V: View<Dom = D>,
    {
        self.doms.value(x)
    }

    pub fn min<I, V>(&self, view: I) -> V::Value
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        self.doms.min(view)
    }

    pub fn max<I, V>(&self, view: I) -> V::Value
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        self.doms.max(view)
    }

    pub fn assign<I, V>(&mut self, view: I, value: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let x = view.into();
        let event = self.doms.assign(x, value)?;
        if let Some(index) = x.index() {
            self.events.push((index, event));
        }
        Ok(event)
    }

    pub fn remove<I, V>(&mut self, view: I, value: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let x = view.into();
        let event = self.doms.remove(x, value)?;
        if let Some(index) = x.index() {
            self.events.push((index, event));
        }
        Ok(event)
    }

    pub fn adjust_min<I, V>(&mut self, view: I, new_min: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let x = view.into();
        let event = self.doms.adjust_min(x, new_min)?;
        if let Some(index) = x.index() {
            self.events.push((index, event));
        }
        Ok(event)
    }

    pub fn adjust_max<I, V>(&mut self, view: I, new_max: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let x = view.into();
        let event = self.doms.adjust_max(x, new_max)?;
        if let Some(index) = x.index() {
            self.events.push((index, event));
        }
        Ok(event)
    }

    pub fn intersect<I, V>(&mut self, x: Var<V>, values: I) -> Result<ModEvent, Error>
    where
        I: IntoIterator<Item = V::Value>,
        I::IntoIter: DoubleEndedIterator,
        V: View<Dom = D>,
    {
        unimplemented!()
    }
}

/// Lightweight id referring to a propagator in a `Store`.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Token {
    pub(crate) index: usize,
}

impl Token {
    pub fn new(index: usize) -> Self {
        Self { index }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum PropState {
    Entailed,
    Idle {
        cost: Cost,
    },
    InQueue {
        head: QueueItem,
        tail: QueueItem,
        cost: Cost,
        first_time: bool,
    },
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum QueueItem {
    Prop(usize),
    Sentinel(usize),
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct Sentinel {
    head: QueueItem,
    tail: QueueItem,
}

#[derive(Clone, Debug)]
pub struct Queue {
    states: Vec<PropState>,
    sentinels: Vec<Sentinel>,
}

impl Queue {
    pub fn verify_internal_state(&self) {
        let mut in_all_queues = 0;

        for i in 0..NUM_COSTS {
            let mut forward_count = 0;
            let mut head = QueueItem::Sentinel(i);
            let end = head;
            while self.get_tail(head) != end {
                head = self.get_tail(head);
                forward_count += 1;
            }

            let mut backward_count = 0;
            let mut head = QueueItem::Sentinel(i);
            let end = head;
            while self.get_head(head) != end {
                head = self.get_head(head);
                backward_count += 1;
            }

            assert_eq!(forward_count, backward_count);

            in_all_queues += forward_count;
        }

        assert_eq!(
            self.states
                .iter()
                .filter(|state| match state {
                    PropState::InQueue { .. } => true,
                    _ => false,
                }).count(),
            in_all_queues
        );
    }

    fn get_head(&self, queue_item: QueueItem) -> QueueItem {
        match queue_item {
            QueueItem::Prop(index) => match self.states[index] {
                PropState::InQueue { head, .. } => head,
                _ => unreachable!(),
            },
            QueueItem::Sentinel(index) => self.sentinels[index].head,
        }
    }

    fn get_tail(&self, queue_item: QueueItem) -> QueueItem {
        match queue_item {
            QueueItem::Prop(index) => match self.states[index] {
                PropState::InQueue { tail, .. } => tail,
                _ => unreachable!(),
            },
            QueueItem::Sentinel(index) => self.sentinels[index].tail,
        }
    }

    fn set_head(&mut self, queue_item: QueueItem, new_head: QueueItem) {
        match queue_item {
            QueueItem::Prop(index) => {
                if let PropState::InQueue { ref mut head, .. } = self.states[index] {
                    *head = new_head;
                }
            }
            QueueItem::Sentinel(index) => self.sentinels[index].head = new_head,
        }
    }

    fn set_tail(&mut self, queue_item: QueueItem, new_tail: QueueItem) {
        match queue_item {
            QueueItem::Prop(index) => {
                if let PropState::InQueue { ref mut tail, .. } = self.states[index] {
                    *tail = new_tail;
                }
            }
            QueueItem::Sentinel(index) => self.sentinels[index].tail = new_tail,
        }
    }

    pub fn new() -> Self {
        let sentinels = (0..NUM_COSTS)
            .map(|i| Sentinel {
                head: QueueItem::Sentinel(i),
                tail: QueueItem::Sentinel(i),
            }).collect();
        Self {
            states: vec![],
            sentinels,
        }
    }

    pub fn add_one(&mut self, cost: Cost) {
        self.states.push(PropState::Idle { cost });
    }

    pub fn entail(&mut self, prop_token: Token) {
        // remove prop from queue before entailing
        self.idle(prop_token);
        self.states[prop_token.index] = PropState::Entailed;
    }

    pub fn idle(&mut self, prop_token: Token) {
        if let PropState::InQueue {
            head, tail, cost, ..
        } = self.states[prop_token.index]
        {
            self.set_tail(head, tail);
            self.set_head(tail, head);
            self.states[prop_token.index] = PropState::Idle { cost };
        }
    }

    pub fn push(&mut self, prop_token: Token, first_time: bool) {
        match self.states[prop_token.index] {
            // if already in queue, do nothing
            PropState::InQueue { .. } => {}

            // if idle, insert at the end of the correct queue
            PropState::Idle { cost } => {
                let new_item = QueueItem::Prop(prop_token.index);
                // safe because prop is idle so it's not entailed
                let sentinel = QueueItem::Sentinel(cost as usize);
                self.states[prop_token.index] = PropState::InQueue {
                    head: self.get_head(sentinel),
                    tail: sentinel,
                    cost,
                    first_time: first_time,
                };
                self.set_tail(self.get_head(sentinel), new_item);
                self.set_head(sentinel, new_item);
            }

            // do nothing if entailed
            PropState::Entailed => {}
        }
    }

    pub fn head(&mut self) -> Option<(Token, bool)> {
        for i in 0..NUM_COSTS {
            let sentinel = QueueItem::Sentinel(i);
            match self.get_tail(sentinel) {
                QueueItem::Prop(index) => match self.states[index] {
                    PropState::InQueue { first_time, .. } => {
                        let token = Token::new(index);
                        return Some((token, first_time));
                    }
                    _ => unreachable!(),
                },
                QueueItem::Sentinel(_) => continue,
            }
        }
        None
    }
}

/// Structure that owns propagators and the propagator queue.
#[derive(Clone, Debug)]
pub struct Store<D>
where
    D: var::Domain,
{
    props: Vec<Option<Box<dyn Propagate<Dom = D>>>>,
}

impl<D> Store<D>
where
    D: var::Domain,
{
    pub fn new() -> Self {
        Self { props: vec![] }
    }

    pub fn add(&mut self, prop: Box<dyn Propagate<Dom = D>>) -> Token {
        let token = Token::new(self.props.len());
        self.props.push(Some(prop));
        token
    }

    pub fn rewrite(&mut self, token: Token, new_prop: Box<dyn Propagate<Dom = D>>) {
        if let Some(ref mut prop) = self.props[token.index] {
            *prop = new_prop;
        }
    }

    /// Returns the cost of the propagator associated with the token, or `None` if the propagator is entailed.
    pub fn cost(&self, prop_token: Token) -> Option<Cost> {
        if let Some(ref prop) = self.props[prop_token.index] {
            Some(prop.cost())
        } else {
            None
        }
    }

    pub fn propagate(
        &mut self,
        prop_token: Token,
        ctx: &mut Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<Status<D>, Error> {
        if let Some(ref mut prop) = self.props[prop_token.index] {
            prop.propagate(ctx, first_time)
        } else {
            Ok(Status::Entailed)
        }
    }

    pub fn entail(&mut self, prop_token: Token) {
        self.props[prop_token.index].take();
    }
}
