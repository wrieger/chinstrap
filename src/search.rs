pub mod branch;

use self::branch::{Branch, ValStrategy, VarStrategy, VarValBrancher};

use crate::{
    prop::{self, Propagate},
    types::{Error, Int},
    var,
};

#[derive(Clone, Debug)]
pub struct Solution<D>
where
    D: var::Domain,
{
    state: State<D>,
}

impl<D> Solution<D>
where
    D: var::Domain,
{
    fn new(state: State<D>) -> Self {
        Self { state }
    }

    pub fn val<V>(&self, x: var::Var<V>) -> V::Value
    where
        V: var::View<Dom = D>,
    {
        self.state.doms().value(x).unwrap()
    }
}

pub enum Status {
    Failed,
    Solved,
    Unsolved,
}

#[derive(Clone, Debug)]
pub struct State<D>
where
    D: var::Domain,
{
    vars: var::Store<D>,
    var_array: Vec<var::IntVar<D>>,
    props: prop::Store<D>,
    queue: prop::Queue,
}

impl<D> State<D>
where
    D: var::Domain,
{
    pub fn new() -> Self {
        Self {
            vars: var::Store::new(),
            props: prop::Store::new(),
            queue: prop::Queue::new(),
            var_array: vec![],
        }
    }

    pub fn doms(&self) -> &var::Store<D> {
        &self.vars
    }

    pub fn add_dom(&mut self, dom: D) -> var::IntVar<D> {
        let var = self.vars.add(dom);
        self.var_array.push(var);
        var
    }

    pub fn add_prop(&mut self, prop: Box<dyn Propagate<Dom = D>>) -> prop::Token {
        let token = self.props.add(prop);
        self.queue.add_one(self.props.cost(token).unwrap());
        self.queue.push(token, true);
        token
    }

    pub fn apply_decision<B, V>(&mut self, decision: B) -> Result<(), Error>
    where
        B: std::borrow::Borrow<Decision<V>>,
        V: var::View<Dom = D, Value = Int>,
    {
        match decision.borrow() {
            Decision::Eq(x, val) => {
                let event = self.vars.assign(*x, *val)?;
                for token in self.vars.trigger(*x, event).unwrap() {
                    self.queue.push(token, false);
                }
            }
            Decision::Neq(x, val) => {
                let event = self.vars.remove(*x, *val)?;
                for token in self.vars.trigger(*x, event).unwrap() {
                    self.queue.push(token, false);
                }
            }
            Decision::Gt(x, val) => {
                let event = self.vars.adjust_min(*x, *val + 1)?;
                for token in self.vars.trigger(*x, event).unwrap() {
                    self.queue.push(token, false);
                }
            }
            Decision::Lt(x, val) => {
                let event = self.vars.adjust_max(*x, *val - 1)?;
                for token in self.vars.trigger(*x, event).unwrap() {
                    self.queue.push(token, false);
                }
            }
        };
        Ok(())
    }

    pub fn branch_status(&self, vars: &Vec<var::IntVar<D>>) -> Status {
        for &x in vars {
            if self.vars.size(x) > 1 {
                return Status::Unsolved;
            } else if self.vars.is_empty(x) {
                return Status::Failed;
            } else {
                continue;
            }
        }
        Status::Solved
    }

    pub fn status(&self) -> Status {
        for x in self.vars.vars() {
            if self.vars.size(x) > 1 {
                return Status::Unsolved;
            } else if self.vars.is_empty(x) {
                return Status::Failed;
            } else {
                continue;
            }
        }
        Status::Solved
    }

    pub fn propagate(&mut self) -> Result<(), Error> {
        while let Some((token, first_time)) = self.queue.head() {
            let mut collected_events = vec![];
            let mut ctx = prop::Context::new(token, &mut self.vars, &mut collected_events);
            match self.props.propagate(token, &mut ctx, first_time)? {
                prop::Status::Entailed => {
                    self.props.entail(token);
                    self.queue.entail(token);
                    for &(index, event) in &collected_events {
                        for token in self
                            .vars
                            .trigger(self.var_array[index], event)
                            .unwrap()
                            .filter(|&t| t != token)
                        {
                            self.queue.push(token, false);
                        }
                    }
                }
                prop::Status::Fixed => {
                    self.queue.idle(token);
                    for &(index, event) in &collected_events {
                        for token in self
                            .vars
                            .trigger(self.var_array[index], event)
                            .unwrap()
                            .filter(|&t| t != token)
                        {
                            self.queue.push(token, false);
                        }
                    }
                }
                prop::Status::Unsure => {
                    self.queue.idle(token);
                    for &(index, event) in &collected_events {
                        for t in self.vars.trigger(self.var_array[index], event).unwrap() {
                            self.queue.push(t, false);
                        }
                    }
                }
                prop::Status::Rewrite(new_prop) => {
                    // TODO set cost somehow
                    self.props.rewrite(token, new_prop);
                    self.queue.idle(token);
                    self.queue.push(token, true);
                }
            }
        }
        Ok(())
    }

    pub fn solve(
        self,
        branch_vars: Vec<var::IntVar<D>>,
        var_strat: VarStrategy,
        val_strat: ValStrategy,
    ) -> Solve<VarValBrancher<D>, D> {
        Solve {
            states: vec![self],
            brancher: VarValBrancher::new(var_strat, val_strat),
            branch_vars,
            explored: 0,
            failures: 0,
        }
    }

    pub fn minimize<V>(
        self,
        to_minimize: var::Var<V>,
        branch_vars: Vec<var::IntVar<D>>,
        var_strat: VarStrategy,
        val_strat: ValStrategy,
    ) -> Minimize<VarValBrancher<D>, D, V>
    where
        V: var::View<Dom = D, Value = Int>,
    {
        Minimize {
            states: vec![self],
            brancher: VarValBrancher::new(var_strat, val_strat),
            branch_vars,
            explored: 0,
            failures: 0,
            best_min: None,
            to_minimize,
        }
    }
}

#[derive(Debug)]
pub enum Decision<V>
where
    V: var::View<Value = Int>,
{
    Eq(var::Var<V>, Int),
    Neq(var::Var<V>, Int),
    Lt(var::Var<V>, Int),
    Gt(var::Var<V>, Int),
}

impl<V> Clone for Decision<V>
where
    V: var::View<Value = Int>,
{
    fn clone(&self) -> Self {
        match self {
            Decision::Eq(x, val) => Decision::Eq(*x, *val),
            Decision::Neq(x, val) => Decision::Neq(*x, *val),
            Decision::Lt(x, val) => Decision::Lt(*x, *val),
            Decision::Gt(x, val) => Decision::Gt(*x, *val),
        }
    }
}

pub struct Minimize<B, D, V>
where
    B: Branch,
    D: var::Domain,
    V: var::View,
{
    states: Vec<State<D>>,
    brancher: B,
    branch_vars: Vec<var::IntVar<D>>,
    to_minimize: var::Var<V>,
    best_min: Option<Int>,
    explored: usize,
    failures: usize,
}

impl<B, D, V> Iterator for Minimize<B, D, V>
where
    B: Branch<Dom = D>,
    D: var::Domain,
    V: var::View<Dom = D, Value = Int>,
{
    type Item = Solution<D>;

    fn next(&mut self) -> Option<Solution<D>> {
        while let Some(mut state) = self.states.pop() {
            self.explored += 1;
            match self.best_min {
                None => {}
                Some(best_min) => {
                    if state
                        .apply_decision(Decision::Lt(self.to_minimize, best_min))
                        .is_err()
                    {
                        self.failures += 1;
                        continue;
                    }
                }
            }
            if state.propagate().is_err() {
                self.failures += 1;
                continue;
            }
            match state.branch_status(&self.branch_vars) {
                Status::Failed => unreachable!(),
                Status::Solved => {
                    let soln = Solution::new(state);
                    self.best_min = Some(soln.val(self.to_minimize));
                    return Some(soln);
                }
                Status::Unsolved => {
                    let (left_dec, right_dec) = self.brancher.branch(&state, &self.branch_vars);

                    let mut right_state = state.clone();
                    right_state.apply_decision(right_dec).unwrap();
                    self.states.push(right_state);

                    state.apply_decision(left_dec).unwrap();
                    self.states.push(state);
                }
            }
        }
        None
    }
}

pub struct Solve<B, D>
where
    B: Branch,
    D: var::Domain,
{
    states: Vec<State<D>>,
    brancher: B,
    branch_vars: Vec<var::IntVar<D>>,
    explored: usize,
    failures: usize,
}

impl<B, D> Iterator for Solve<B, D>
where
    B: Branch<Dom = D>,
    D: var::Domain,
{
    type Item = Solution<D>;

    fn next(&mut self) -> Option<Solution<D>> {
        while let Some(mut state) = self.states.pop() {
            self.explored += 1;
            if state.propagate().is_err() {
                self.failures += 1;
                continue;
            }
            match state.status() {
                Status::Failed => unreachable!(),
                Status::Solved => {
                    return Some(Solution::new(state));
                }
                Status::Unsolved => {
                    let (left_dec, right_dec) = self.brancher.branch(&state, &self.branch_vars);

                    let mut right_state = state.clone();
                    right_state.apply_decision(right_dec).unwrap();
                    self.states.push(right_state);

                    state.apply_decision(left_dec).unwrap();
                    self.states.push(state);
                }
            }
        }
        println!("failures: {}", self.failures);
        println!("explored: {}", self.explored);
        None
    }
}
