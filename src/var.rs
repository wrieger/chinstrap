//! Variable module.

pub mod domains;
pub mod views;

use std::ops::{BitOr, BitOrAssign};

use crate::{
    prop::{self, Token},
    types::{Error, Int, Value},
};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ModEvent {
    /// The domain was reduced to a single value.
    Assigned,

    /// The minimum or maximum of the domain changed, and the domain wasn't assigned.
    BoundsChanged,

    /// The interior of the domain changed (not the bounds).
    Reduced,

    /// The domain was unchanged.
    Unchanged,
}

impl ModEvent {
    pub fn union(self, other: Self) -> Self {
        match (self, other) {
            (ModEvent::Assigned, _) => ModEvent::Assigned,
            (_, ModEvent::Assigned) => ModEvent::Assigned,

            (ModEvent::BoundsChanged, _) => ModEvent::BoundsChanged,
            (_, ModEvent::BoundsChanged) => ModEvent::BoundsChanged,

            (ModEvent::Reduced, _) => ModEvent::Reduced,
            (_, ModEvent::Reduced) => ModEvent::Reduced,

            _ => ModEvent::Unchanged,
        }
    }
}

impl BitOr<ModEvent> for ModEvent {
    type Output = ModEvent;

    fn bitor(self, rhs: ModEvent) -> ModEvent {
        self.union(rhs)
    }
}

impl BitOrAssign<ModEvent> for ModEvent {
    fn bitor_assign(&mut self, rhs: ModEvent) {
        *self = self.union(rhs);
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ModTrigger {
    /// Triggered by `ModEvent::Assigned`.
    OnAssigned,

    /// Triggered by `ModEvent::Assigned` and `ModEvent::BoundsChanged`.
    OnBoundsChanged,

    /// Triggered by `ModEvent::Assigned`, `ModEvent::BoundsChanged`, and `ModEvent::Reduced`.
    OnReduced,
}

/// The set of possible values a variable can take.
///
/// # Notes
///
/// Domains support only removal operations, not insertions.
/// There is never any reason to insert a value into a domain after it's created, since the goal is to reduce every domain to a single value.
///
/// It's an unfortunate consequence of Rust's type system that the iterator returned from `values()` must be heap allocated in a `Box`.
/// This is because it's impossible to express the lifetime relationship between `self` and the returned iterator in current Rust using associated types.
/// Once [generic associated types](https://github.com/rust-lang/rust/issues/44265) land this could change.
pub trait Domain: Clone + std::fmt::Debug {
    /// Creates a new domain equivalent to the range `min..=max`.
    ///
    /// It's undefined behavior to create a domain from an empty range (e.g. `1..0`).
    fn from_bounds(min: Int, max: Int) -> Self;

    /// Creates a new domain from the given values.
    ///
    /// It's undefined behavior to create a domain from an empty iterator of values.
    fn from_values<I>(values: I) -> Self
    where
        I: IntoIterator<Item = Int>;

    /// Returns a double-ended iterator over the values in the domain.
    ///
    /// The (forward) iteration _must_ be in ascending order.
    fn values<'a>(&'a self) -> Box<dyn DoubleEndedIterator<Item = Int> + 'a>;

    /// Returns the number of elements in the domain.
    fn size(&self) -> usize;

    /// Checks if the size of the domain is 0.
    fn is_empty(&self) -> bool;

    /// Checks if the size of the domain is 1.
    fn is_assigned(&self) -> bool;

    /// Checks if the domain contains the given value.
    ///
    /// It's undefined behavior to call `contains()` on an empty domain.
    fn contains(&self, value: Int) -> bool;

    /// Returns `Some(val)` if the domain is assigned to `val` and `None` otherwise.
    ///
    /// It's undefined behavior to call `value()` on an empty domain.
    fn value(&self) -> Option<Int>;

    /// Returns a random value from the domain.
    ///
    /// It's undefined behavior to call `random()` on an empty domain.
    fn random(&self) -> Int;

    /// Returns the maximum value in the domain.
    ///
    /// It's undefined behavior to call `min()` on an empty domain.
    fn min(&self) -> Int;

    /// Returns the maximum value in the domain.
    ///
    /// It's undefined behavior to call `max()` on an empty domain.
    fn max(&self) -> Int;

    fn assign(&mut self, value: Int) -> Result<ModEvent, Error>;

    /// Removes the value from the domain.
    ///
    /// Returns `Err(error)` if the call to `remove()` caused the domain to become empty.
    /// Otherwise, it returns `Ok(mod_event)`, where `mod_event` describes how the domain was modified.
    ///
    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::ModEvent;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut dom = BitSetDom::from_bounds(-1, 1);
    /// // dom is { -1, 0, 1 }
    ///
    /// assert_eq!(dom.remove(0), Ok(ModEvent::Reduced));
    /// // dom is { -1, 1 }
    /// ```
    fn remove(&mut self, value: Int) -> Result<ModEvent, Error>;

    /// Removes all values less than the given value from the domain.
    ///
    /// Returns `Err(error)` if the call to `adjust_min()` caused the domain to become empty.
    /// Otherwise, it returns `Ok(mod_event)`, where `mod_event` describes how the domain was modified.
    ///
    /// Note that this method can never return `Ok(ModEvent::Reduced)`.
    ///
    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::ModEvent;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut dom = BitSetDom::from_bounds(0, 5);
    /// // dom is { 0, 1, 2, 3, 4, 5 }
    ///
    /// assert_eq!(dom.adjust_min(3), Ok(ModEvent::BoundsChanged));
    /// // dom is { 3, 4, 5 }
    /// ```
    fn adjust_min(&mut self, new_min: Int) -> Result<ModEvent, Error>;

    /// Removes all values greater than the given value from the domain.
    ///
    /// Returns `Err(error)` if the call to `adjust_maax()` caused the domain to become empty.
    /// Otherwise, it returns `Ok(mod_event)`, where `mod_event` describes how the domain was modified.
    ///
    /// Note that this method can never return `Ok(ModEvent::Reduced)`.
    ///
    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::ModEvent;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut dom = BitSetDom::from_bounds(0, 5);
    /// // dom is { 0, 1, 2, 3, 4, 5 }
    ///
    /// assert_eq!(dom.adjust_max(1), Ok(ModEvent::BoundsChanged));
    /// // dom is { 0, 1 }
    /// ```
    fn adjust_max(&mut self, new_max: Int) -> Result<ModEvent, Error>;

    fn intersect<I>(&mut self, values: I) -> Result<ModEvent, Error>
    where
        I: IntoIterator<Item = Int>;
}

/// View
///
/// # Notes
///
/// While `Domains` have the methods `size()`, `is_empty()`, and `is_assigned()`, views do not.
pub trait View: Copy + std::fmt::Debug {
    type Dom: Domain;
    type Value: Value;

    fn values<'s, 'd>(
        &'s self,
        dom: &'d Self::Dom,
    ) -> Box<DoubleEndedIterator<Item = Self::Value> + 'd>
    where
        's: 'd;
    fn contains(&self, dom: &Self::Dom, value: Self::Value) -> bool;
    fn value(&self, dom: &Self::Dom) -> Option<Self::Value>;
    fn random(&self, dom: &Self::Dom) -> Self::Value;
    fn min(&self, dom: &Self::Dom) -> Self::Value;
    fn max(&self, dom: &Self::Dom) -> Self::Value;
    fn assign(&self, dom: &mut Self::Dom, value: Self::Value) -> Result<ModEvent, Error>;
    fn remove(&self, dom: &mut Self::Dom, value: Self::Value) -> Result<ModEvent, Error>;
    fn adjust_min(&self, dom: &mut Self::Dom, new_min: Self::Value) -> Result<ModEvent, Error>;
    fn adjust_max(&self, dom: &mut Self::Dom, new_max: Self::Value) -> Result<ModEvent, Error>;
    fn intersect<I>(&self, dom: &mut Self::Dom, values: I) -> Result<ModEvent, Error>
    where
        I: IntoIterator<Item = Self::Value>,
        I::IntoIter: DoubleEndedIterator;
}

/// The base integer variable type returned by a `Store`.
pub type IntVar<D> = Var<views::BaseView<D>>;

/// Integer variable representing a constant `c`.
pub type ConstIntVar<D> = Var<views::ConstantView<D, Int>>;

/// Integer variable representing `a * x + b`, `a > 0`, where `x` is an `IntVar`.
pub type PosAffineIntVar<D> = Var<views::OffsetView<views::ScaleView<views::BaseView<D>>>>;

/// Integer variable representing `-a * x + b`, `a > 0`, where `x` is an `IntVar`.
pub type NegAffineIntVar<D> =
    Var<views::OffsetView<views::MinusView<views::ScaleView<views::BaseView<D>>>>>;

pub fn const_int_var<D>(c: Int) -> ConstIntVar<D>
where
    D: Domain,
{
    Var {
        index: None,
        view: views::ConstantView::new(c),
    }
}

impl<D> PartialEq for IntVar<D>
where
    D: Domain,
{
    fn eq(&self, rhs: &Self) -> bool {
        self.index == rhs.index
    }
}
impl<D> Eq for IntVar<D> where D: Domain {}

impl<D> PartialOrd for IntVar<D>
where
    D: Domain,
{
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        self.index.partial_cmp(&rhs.index)
    }
}
impl<D> Ord for IntVar<D>
where
    D: Domain,
{
    fn cmp(&self, rhs: &Self) -> std::cmp::Ordering {
        self.index.cmp(&rhs.index)
    }
}

/// A variable in a constraint satisfaction problem.
///
/// A `Var` is a lightweight reference to a domain in a `Store`, along with an associated `View`.
///
/// There are two ways to create a new `Var`:
/// - Add a domain to a `Store`.
/// - Using the function [`const_int_var()`](const_int_var).
#[derive(Clone, Copy, Debug)]
pub struct Var<V>
where
    V: View,
{
    // `None` means V is `ConstantView`, since a constant view doesn't depend on any domain.
    index: Option<usize>,
    view: V,
}

impl<V> Var<V>
where
    V: View,
{
    pub fn index(&self) -> Option<usize> {
        self.index
    }
    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::Store;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut store = Store::new();
    /// let x = store.add(BitSetDom::from_bounds(2, 8));
    /// assert_eq!(store.min(x), 2);
    ///
    /// let y = x.offset(7);
    /// assert_eq!(store.min(y), 9);
    /// ```
    pub fn offset(&self, offset: Int) -> Var<views::OffsetView<V>>
    where
        V: View<Value = Int>,
    {
        Var {
            index: self.index,
            view: views::OffsetView::new(self.view, offset),
        }
    }

    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::Store;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut store = Store::new();
    /// let x = store.add(BitSetDom::from_bounds(4, 6));
    /// assert_eq!(store.min(x), 4);
    ///
    /// let y = x.scale(3);
    /// assert_eq!(store.min(y), 12);
    /// ```
    pub fn scale(&self, scale: Int) -> Var<views::ScaleView<V>>
    where
        V: View<Value = Int>,
    {
        assert!(scale > 0);
        Var {
            index: self.index,
            view: views::ScaleView::new(self.view, scale),
        }
    }

    /// # Examples
    ///
    /// ```
    /// use chinstrap::var::Domain;
    /// use chinstrap::var::Store;
    /// use chinstrap::var::domains::BitSetDom;
    ///
    /// let mut store = Store::new();
    /// let x = store.add(BitSetDom::from_bounds(-3, 5));
    /// assert_eq!(store.min(x), -3);
    ///
    /// let y = x.minus();
    /// assert_eq!(store.min(y), -5);
    /// ```
    pub fn minus(&self) -> Var<views::MinusView<V>>
    where
        V: View<Value = Int>,
    {
        Var {
            index: self.index,
            view: views::MinusView::new(self.view),
        }
    }
}

impl<D, T> From<T> for Var<views::ConstantView<D, T>>
where
    D: Domain,
    T: Value,
{
    fn from(val: T) -> Self {
        Var {
            index: None,
            view: views::ConstantView::new(val),
        }
    }
}

/// A structure to manage variable domains and variable dependencies.
#[derive(Clone, Debug)]
pub struct Store<D>
where
    D: Domain,
{
    doms: Vec<D>,
    const_dom: D,
    handlers: Vec<Handler>,
}

impl<D> Store<D>
where
    D: Domain,
{
    pub fn new() -> Self {
        Self {
            doms: vec![],
            const_dom: D::from_bounds(1, 0),
            handlers: vec![],
        }
    }

    pub fn vars(&self) -> impl Iterator<Item = IntVar<D>> {
        (0..self.doms.len()).map(|i| Var {
            index: Some(i),
            view: views::BaseView::new(),
        })
    }

    pub fn add(&mut self, dom: D) -> IntVar<D> {
        let index = self.doms.len();
        self.doms.push(dom);
        self.handlers.push(Handler::new());
        Var {
            index: Some(index),
            view: views::BaseView::new(),
        }
    }

    pub fn subscribe<I, V>(&mut self, view: I, prop_token: Token, trigger: ModTrigger)
    where
        I: Into<Var<V>>,
        V: View,
    {
        let view = view.into();
        if let Some(index) = view.index {
            self.handlers[index].subscribe(prop_token, trigger);
        }
    }

    pub fn unsubscribe<I, V>(&mut self, view: I, prop_token: Token)
    where
        I: Into<Var<V>>,
        V: View,
    {
        let view = view.into();
        if let Some(index) = view.index {
            self.handlers[index].unsubscribe(prop_token);
        }
    }

    pub fn trigger<'a, I, V>(
        &'a self,
        view: I,
        mod_event: ModEvent,
    ) -> Option<impl Iterator<Item = Token> + 'a>
    where
        I: Into<Var<V>>,
        V: View,
    {
        let view = view.into();
        if let Some(index) = view.index {
            Some(self.handlers[index].trigger(mod_event))
        } else {
            None
        }
    }

    pub fn values<'a, 'v, V>(
        &'a self,
        x: &'v Var<V>,
    ) -> Box<dyn DoubleEndedIterator<Item = V::Value> + 'v>
    where
        V: View<Dom = D>,
        'a: 'v,
    {
        if let Some(index) = x.index {
            x.view.values(&self.doms[index])
        } else {
            x.view.values(&self.doms[0])
        }
    }

    /// Returns the size of the domain of the given `Var`.
    pub fn size<V, X>(&self, x: X) -> usize
    where
        V: View<Dom = D>,
        X: Into<Var<V>>,
    {
        let x = x.into();
        if let Some(index) = x.index {
            self.doms[index].size()
        } else {
            // constant view has size 1
            1
        }
    }

    pub fn is_empty<I, V>(&self, view: I) -> bool
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            self.doms[index].is_empty()
        } else {
            // constant view is never empty
            false
        }
    }

    pub fn is_assigned<I, V>(&self, view: I) -> bool
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            self.doms[index].is_assigned()
        } else {
            // constant view is always assigned
            true
        }
    }

    pub fn contains<V>(&self, x: Var<V>, value: V::Value) -> bool
    where
        V: View<Dom = D>,
    {
        if let Some(index) = x.index {
            x.view.contains(&self.doms[index], value)
        } else {
            x.view.contains(&self.const_dom, value)
        }
    }

    pub fn value<V>(&self, x: Var<V>) -> Option<V::Value>
    where
        V: View<Dom = D>,
    {
        if let Some(index) = x.index {
            x.view.value(&self.doms[index])
        } else {
            x.view.value(&self.const_dom)
        }
    }

    pub fn random<I, V>(&self, view: I) -> V::Value
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.random(&self.doms[index])
        } else {
            view.view.random(&self.const_dom)
        }
    }

    pub fn min<I, V>(&self, view: I) -> V::Value
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.min(&self.doms[index])
        } else {
            view.view.min(&self.const_dom)
        }
    }

    pub fn max<I, V>(&self, view: I) -> V::Value
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.max(&self.doms[index])
        } else {
            view.view.max(&self.const_dom)
        }
    }

    pub fn assign<I, V>(&mut self, view: I, value: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.assign(&mut self.doms[index], value)
        } else {
            view.view.assign(&mut self.const_dom, value)
        }
    }

    pub fn remove<I, V>(&mut self, view: I, value: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.remove(&mut self.doms[index], value)
        } else {
            view.view.remove(&mut self.const_dom, value)
        }
    }

    pub fn adjust_min<I, V>(&mut self, view: I, new_min: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.adjust_min(&mut self.doms[index], new_min)
        } else {
            view.view.adjust_min(&mut self.const_dom, new_min)
        }
    }

    pub fn adjust_max<I, V>(&mut self, view: I, new_max: V::Value) -> Result<ModEvent, Error>
    where
        I: Into<Var<V>>,
        V: View<Dom = D>,
    {
        let view = view.into();
        if let Some(index) = view.index {
            view.view.adjust_max(&mut self.doms[index], new_max)
        } else {
            view.view.adjust_max(&mut self.const_dom, new_max)
        }
    }

    pub fn intersect<I, V>(
        &mut self,
        x: Var<V>,
        values: I,
        queue: &mut prop::Queue,
        to_ignore: Option<prop::Token>,
    ) -> Result<(ModEvent, bool), Error>
    where
        I: IntoIterator<Item = V::Value>,
        I::IntoIter: DoubleEndedIterator,
        V: View<Dom = D>,
    {
        if let Some(index) = x.index {
            x.view
                .intersect(&mut self.doms[index], values)
                .map(|event| {
                    let mut prop_was_triggered = false;
                    for token in self.handlers[index].trigger(event) {
                        if to_ignore == Some(token) {
                            prop_was_triggered = true;
                        } else {
                            queue.push(token, false);
                        }
                    }
                    (event, prop_was_triggered)
                })
        } else {
            x.view
                .intersect(&mut self.const_dom, values)
                .map(|event| (event, false))
        }
    }
}

#[derive(Clone, Debug)]
struct Handler {
    subs: Vec<prop::Token>,
    bounds_changed_offset: usize,
    assigned_offset: usize,
}

impl Handler {
    fn new() -> Self {
        Self {
            subs: vec![],
            bounds_changed_offset: 0,
            assigned_offset: 0,
        }
    }

    fn subscribe(&mut self, prop_token: Token, trigger: ModTrigger) {
        self.unsubscribe(prop_token);
        match trigger {
            ModTrigger::OnAssigned => {
                self.subs.push(prop_token);
            }
            ModTrigger::OnBoundsChanged => {
                self.subs.insert(self.assigned_offset, prop_token);
                self.assigned_offset += 1;
            }
            ModTrigger::OnReduced => {
                self.subs.insert(self.bounds_changed_offset, prop_token);
                self.assigned_offset += 1;
                self.bounds_changed_offset += 1;
            }
        }
    }

    fn unsubscribe(&mut self, prop_token: Token) {
        if let Some(index) = self.subs.iter().position(|&t| t == prop_token) {
            self.subs.remove(index);
            if index < self.bounds_changed_offset {
                self.assigned_offset -= 1;
                self.bounds_changed_offset -= 1;
            } else if index < self.assigned_offset {
                self.assigned_offset -= 1;
            }
        }
    }

    fn trigger<'a>(&'a self, event: ModEvent) -> impl Iterator<Item = prop::Token> + 'a {
        let to_take = match event {
            ModEvent::Unchanged => 0,
            ModEvent::Reduced => self.bounds_changed_offset,
            ModEvent::BoundsChanged => self.assigned_offset,
            ModEvent::Assigned => self.subs.len(),
        };
        self.subs.iter().take(to_take).cloned()
    }
}
