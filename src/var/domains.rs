//! Domain bases.

use rand::{thread_rng, Rng};

use crate::{
    types::{Error, Int},
    var::{Domain, ModEvent},
};

const BLOCK_SIZE: u32 = 32;

#[derive(Clone, Debug)]
struct BitSet {
    blocks: Vec<u32>,
    max: u32,
}

impl BitSet {
    fn new_full(max: u32) -> Self {
        let num_blocks = max / BLOCK_SIZE + 1;
        let blocks = vec![!0; num_blocks as usize];
        Self { blocks, max }
    }

    fn new_empty(max: u32) -> Self {
        let num_blocks = max / BLOCK_SIZE + 1;
        let blocks = vec![0; num_blocks as usize];
        Self { blocks, max }
    }

    fn insert(&mut self, value: u32) {
        if value <= self.max {
            self.blocks[(value / BLOCK_SIZE) as usize] |= 1 << (value % BLOCK_SIZE);
        }
    }

    fn contains(&self, value: u32) -> bool {
        if value > self.max {
            false
        } else {
            (self.blocks[(value / BLOCK_SIZE) as usize] >> value % BLOCK_SIZE) & 1 == 1
        }
    }

    fn remove(&mut self, value: u32) {
        if value <= self.max {
            self.blocks[(value / BLOCK_SIZE) as usize] &= !(1 << (value % BLOCK_SIZE));
        }
    }

    fn count_between(&self, min: u32, max: u32) -> u32 {
        if min <= max && min <= self.max {
            let max = max.min(self.max);
            let min_block = min / BLOCK_SIZE;
            let max_block = max / BLOCK_SIZE;
            let min_offset = min % BLOCK_SIZE;
            let max_offset = max % BLOCK_SIZE;
            let min_mask = !0 << min_offset;
            let max_mask = !0 >> BLOCK_SIZE - max_offset - 1;
            if min_block == max_block {
                let mask = min_mask & max_mask;
                (self.blocks[min_block as usize] & mask).count_ones()
            } else {
                let min_block_count = (self.blocks[min_block as usize] & min_mask).count_ones();
                let max_block_count = (self.blocks[max_block as usize] & max_mask).count_ones();
                let mut tot = min_block_count + max_block_count;
                for i in (min_block + 1)..max_block {
                    tot += self.blocks[i as usize].count_ones();
                }
                tot
            }
        } else {
            0
        }
    }

    fn next_above(&self, value: u32) -> Option<u32> {
        if value > self.max {
            None
        } else {
            let block = value / BLOCK_SIZE;
            let offset = value % BLOCK_SIZE;
            let to_skip = (self.blocks[block as usize] >> offset).trailing_zeros();
            if to_skip == BLOCK_SIZE {
                self.next_above((block + 1) * BLOCK_SIZE)
            } else {
                if value + to_skip > self.max {
                    None
                } else {
                    Some(value + to_skip)
                }
            }
        }
    }

    fn next_below(&self, value: u32) -> Option<u32> {
        let value = value.min(self.max);
        let block = value / BLOCK_SIZE;
        let offset = value % BLOCK_SIZE;
        let to_skip = (self.blocks[block as usize] << (BLOCK_SIZE - offset - 1)).leading_zeros();
        if to_skip == BLOCK_SIZE {
            if block == 0 {
                None
            } else {
                self.next_below(block * BLOCK_SIZE - 1)
            }
        } else {
            Some(value - to_skip)
        }
    }
}

#[derive(Clone, Debug)]
pub struct BitSetDom {
    values: BitSet,
    size: usize,
    min: Int,
    max: Int,
    offset: Int,
}

impl Domain for BitSetDom {
    fn from_bounds(min: Int, max: Int) -> Self {
        let offset = min;
        let mut values = BitSet::new_full((max - min + 1) as u32);
        Self {
            values,
            size: (max - min + 1) as usize,
            min,
            max,
            offset,
        }
    }

    fn from_values<I>(values: I) -> Self
    where
        I: IntoIterator<Item = Int>,
    {
        let values = values
            .into_iter()
            .collect::<std::collections::BTreeSet<_>>();
        assert!(values.len() > 0);
        let min = values.range(..).next().cloned().unwrap();
        let max = values.range(..).next_back().cloned().unwrap();
        let size = values.len();
        let offset = min;
        let mut set = BitSet::new_empty((max - min + 1) as u32);
        for value in values {
            set.insert((value - offset) as u32);
        }
        Self {
            values: set,
            size,
            min,
            max,
            offset,
        }
    }

    fn values<'a>(&'a self) -> Box<dyn DoubleEndedIterator<Item = Int> + 'a> {
        Box::new(
            (self.min..=self.max).filter(move |&i| self.values.contains((i - self.offset) as u32)),
        )
    }

    #[inline(always)]
    fn size(&self) -> usize {
        self.size
    }

    #[inline(always)]
    fn is_empty(&self) -> bool {
        self.size == 0
    }

    #[inline(always)]
    fn is_assigned(&self) -> bool {
        self.size == 1
    }

    #[inline(always)]
    fn contains(&self, value: Int) -> bool {
        self.min <= value && value <= self.max && self.values.contains((value - self.offset) as u32)
    }

    #[inline(always)]
    fn value(&self) -> Option<Int> {
        if self.size == 1 {
            Some(self.min)
        } else {
            None
        }
    }

    fn random(&self) -> Int {
        let mut rng = thread_rng();
        let n = rng.gen_range(0, self.size());
        self.values().nth(n).unwrap()
    }

    #[inline(always)]
    fn min(&self) -> Int {
        self.min
    }

    #[inline(always)]
    fn max(&self) -> Int {
        self.max
    }

    fn assign(&mut self, value: Int) -> Result<ModEvent, Error> {
        if self.contains(value) {
            if self.size != 1 {
                self.min = value;
                self.max = value;
                self.size = 1;
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::Unchanged)
            }
        } else {
            self.size = 0;
            Err(Error::DomEmpty)
        }
    }

    fn remove(&mut self, value: Int) -> Result<ModEvent, Error> {
        if self.contains(value) {
            self.size -= 1;
            if self.size == 0 {
                Err(Error::DomEmpty)
            } else {
                self.values.remove((value - self.offset) as u32);
                if self.size == 1 {
                    if value == self.min {
                        self.min = self.max;
                    } else {
                        self.max = self.min;
                    }
                    Ok(ModEvent::Assigned)
                } else if value == self.min {
                    self.min = self
                        .values
                        .next_above((self.min - self.offset) as u32)
                        .unwrap() as Int
                        + self.offset;
                    Ok(ModEvent::BoundsChanged)
                } else if value == self.max {
                    self.max = self
                        .values
                        .next_below((self.max - self.offset) as u32)
                        .unwrap() as Int
                        + self.offset;
                    Ok(ModEvent::BoundsChanged)
                } else {
                    Ok(ModEvent::Reduced)
                }
            }
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_min(&mut self, new_min: Int) -> Result<ModEvent, Error> {
        if new_min > self.max {
            self.size = 0;
            Err(Error::DomEmpty)
        } else if new_min == self.max {
            if self.size > 1 {
                self.min = self.max;
                self.size = 1;
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::Unchanged)
            }
        } else if self.min < new_min {
            self.min = self
                .values
                .next_above((new_min - self.offset) as u32)
                .unwrap() as Int
                + self.offset;
            self.size = self.values.count_between(
                (self.min - self.offset) as u32,
                (self.max - self.offset) as u32,
            ) as usize;
            if self.size == 1 {
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::BoundsChanged)
            }
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_max(&mut self, new_max: Int) -> Result<ModEvent, Error> {
        if new_max < self.min {
            self.size = 0;
            Err(Error::DomEmpty)
        } else if new_max == self.min {
            if self.size > 1 {
                self.max = self.min;
                self.size = 1;
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::Unchanged)
            }
        } else if new_max < self.max {
            self.max = self
                .values
                .next_below((new_max - self.offset) as u32)
                .unwrap() as Int
                + self.offset;
            self.size = self.values.count_between(
                (self.min - self.offset) as u32,
                (self.max - self.offset) as u32,
            ) as usize;
            if self.size == 1 {
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::BoundsChanged)
            }
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn intersect<I>(&mut self, values: I) -> Result<ModEvent, Error>
    where
        I: IntoIterator<Item = Int>,
    {
        unimplemented!()
        //     let mut i = self.min;
        //     let prev_size = self.size;
        //     let prev_min = self.min;
        //     let prev_max = self.max;
        //     let mut new_size = 0;
        //     let mut new_min = None;
        //     let mut new_max = None;
        //     for value in values
        //         .into_iter()
        //         .filter(|&v| prev_min <= v && v <= prev_max)
        //     {
        //         while i != value {
        //             self.values.remove((i - self.offset) as usize);
        //             if i == prev_max {
        //                 break;
        //             }
        //             i += 1;
        //         }
        //         if !self.contains(i) {
        //             self.values.remove((i - self.offset) as usize);
        //         } else {
        //             new_size += 1;
        //             match new_min {
        //                 None => new_min = Some(i),
        //                 _ => {}
        //             };
        //             new_max = Some(i);
        //         }
        //         i += 1;
        //     }

        //     self.size = new_size;
        //     if self.size > 0 {
        //         let new_min = new_min.unwrap();
        //         let new_max = new_max.unwrap();
        //         self.min = new_min;
        //         self.max = new_max;
        //         if self.size == prev_size {
        //             Ok(ModEvent::Unchanged)
        //         } else {
        //             if self.size == 1 {
        //                 Ok(ModEvent::Assigned)
        //             } else if self.min > prev_min || self.max < prev_max {
        //                 Ok(ModEvent::BoundsChanged)
        //             } else {
        //                 Ok(ModEvent::Reduced)
        //             }
        //         }
        //     } else {
        //         Err(Error::DomEmpty)
        //     }
    }
}

#[derive(Clone, Debug)]
pub struct RangeDom {
    min: Int,
    max: Int,
}

impl Domain for RangeDom {
    fn from_bounds(min: Int, max: Int) -> Self {
        Self { min, max }
    }

    fn from_values<I>(values: I) -> Self
    where
        I: IntoIterator<Item = Int>,
    {
        unimplemented!()
    }

    fn values<'a>(&'a self) -> Box<dyn DoubleEndedIterator<Item = Int> + 'a> {
        unimplemented!()
    }

    fn size(&self) -> usize {
        0.max(self.max - self.min + 1) as usize
    }

    fn is_empty(&self) -> bool {
        self.min > self.max
    }

    fn is_assigned(&self) -> bool {
        self.min == self.max
    }

    fn contains(&self, value: Int) -> bool {
        self.min <= value && value <= self.max
    }

    fn value(&self) -> Option<Int> {
        if self.min == self.max {
            Some(self.min)
        } else {
            None
        }
    }

    fn min(&self) -> Int {
        self.min
    }

    fn max(&self) -> Int {
        self.max
    }

    fn random(&self) -> Int {
        unimplemented!()
    }

    fn intersect<I>(&mut self, values: I) -> Result<ModEvent, Error>
    where
        I: IntoIterator<Item = Int>,
    {
        unimplemented!()
    }

    fn assign(&mut self, value: Int) -> Result<ModEvent, Error> {
        if self.contains(value) {
            if self.min == value {
                Ok(ModEvent::Unchanged)
            } else {
                self.min = value;
                self.max = value;
                Ok(ModEvent::Assigned)
            }
        } else {
            self.min = 1;
            self.max = 0;
            Err(Error::DomEmpty)
        }
    }

    fn remove(&mut self, value: Int) -> Result<ModEvent, Error> {
        if self.contains(value) {
            if self.min == value {
                self.min += 1;
                if self.min == self.max {
                    Ok(ModEvent::Assigned)
                } else {
                    Ok(ModEvent::BoundsChanged)
                }
            } else if self.max == value {
                self.max -= 1;
                if self.min == self.max {
                    Ok(ModEvent::Assigned)
                } else {
                    Ok(ModEvent::BoundsChanged)
                }
            } else {
                panic!()
            }
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_min(&mut self, new_min: Int) -> Result<ModEvent, Error> {
        if new_min > self.max {
            Err(Error::DomEmpty)
        } else {
            self.min = self.min.max(new_min);
            if self.min == self.max {
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::BoundsChanged)
            }
        }
    }

    fn adjust_max(&mut self, new_max: Int) -> Result<ModEvent, Error> {
        if new_max < self.min {
            Err(Error::DomEmpty)
        } else {
            self.max = self.max.min(new_max);
            if self.min == self.max {
                Ok(ModEvent::Assigned)
            } else {
                Ok(ModEvent::BoundsChanged)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod bit_set {
        use super::*;

        #[test]
        fn bit_set() {
            let max = 150;
            let mut bit_set = BitSet::new_full(max);
            for i in 0..=max {
                assert!(bit_set.contains(i));
            }

            for i in (0..=max).filter(|i| i % 5 != 2) {
                bit_set.remove(i);
            }

            for i in 0..=max {
                if i % 5 == 2 {
                    assert!(bit_set.contains(i));
                } else {
                    assert!(!bit_set.contains(i));
                }
            }

            for i in 0..=max {
                for j in i..=max {
                    assert_eq!(
                        bit_set.count_between(i, j),
                        (i..=j).filter(|&i| i % 5 == 2).count() as u32
                    );
                }
            }

            for i in 0..=max {
                assert_eq!(
                    bit_set.next_above(i),
                    if i < 2 {
                        Some(2)
                    } else if i > ((((max - 2) as f32) / 5.).ceil() * 5. + 2.) as u32 - 5 {
                        None
                    } else {
                        Some(((((i - 2) as f32) / 5.).ceil() * 5. + 2.) as u32)
                    }
                );
            }

            for i in 0..=max {
                assert_eq!(
                    bit_set.next_below(i),
                    if i < 2 {
                        None
                    } else {
                        Some(((((i - 2) as f32) / 5.).floor() * 5. + 2.) as u32)
                    }
                );
            }
        }

        /// Asserts that the domain is equivalent to the given values.
        fn assert_dom<D>(dom: &D, values: &[Int])
        where
            D: Domain,
        {
            if values.is_empty() {
                // these are the only methods we can safely use on an empty domain
                assert_eq!(dom.size(), 0);
                assert!(dom.is_empty());
                assert!(!dom.is_assigned());
            } else if values.len() == 1 {
                assert_eq!(dom.size(), 1);
                assert!(!dom.is_empty());
                assert!(dom.is_assigned());
                assert_eq!(dom.min(), values[0]);
                assert_eq!(dom.max(), values[0]);
                assert_eq!(dom.value(), Some(values[0]));
                assert_eq!(dom.values().collect::<Vec<_>>(), values);
            } else {
                assert!(!dom.is_empty());
                assert!(!dom.is_assigned());
                assert_eq!(dom.size(), values.len());
                assert_eq!(dom.min(), *values.iter().min().unwrap());
                assert_eq!(dom.max(), *values.iter().max().unwrap());
                assert_eq!(dom.value(), None);
                assert_eq!(dom.values().collect::<Vec<_>>(), values);
            }
        }

        #[test]
        fn assign() {
            let mut dom = BitSetDom::from_bounds(-11, -9);
            assert_dom(&dom, &[-11, -10, -9]);

            assert_eq!(dom.assign(-10), Ok(ModEvent::Assigned));
            assert_dom(&dom, &[-10]);

            assert_eq!(dom.assign(-10), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[-10]);

            assert_eq!(dom.assign(0), Err(Error::DomEmpty));
            assert_dom(&dom, &[]);
        }

        #[test]
        fn remove() {
            let mut dom = BitSetDom::from_bounds(0, 9);
            assert_dom(&dom, &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

            assert_eq!(dom.remove(-1), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

            assert_eq!(dom.remove(10), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

            assert_eq!(dom.remove(5), Ok(ModEvent::Reduced));
            assert_dom(&dom, &[0, 1, 2, 3, 4, 6, 7, 8, 9]);

            assert_eq!(dom.remove(0), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[1, 2, 3, 4, 6, 7, 8, 9]);

            assert_eq!(dom.remove(9), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[1, 2, 3, 4, 6, 7, 8]);

            assert_eq!(dom.remove(2), Ok(ModEvent::Reduced));
            assert_dom(&dom, &[1, 3, 4, 6, 7, 8]);

            assert_eq!(dom.remove(1), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[3, 4, 6, 7, 8]);

            assert_eq!(dom.remove(7), Ok(ModEvent::Reduced));
            assert_dom(&dom, &[3, 4, 6, 8]);

            assert_eq!(dom.remove(8), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[3, 4, 6]);

            assert_eq!(dom.remove(4), Ok(ModEvent::Reduced));
            assert_dom(&dom, &[3, 6]);

            assert_eq!(dom.remove(4), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[3, 6]);

            assert_eq!(dom.remove(3), Ok(ModEvent::Assigned));
            assert_dom(&dom, &[6]);

            assert_eq!(dom.remove(6), Err(Error::DomEmpty));
            assert_dom(&dom, &[]);
        }

        #[test]
        fn adjust_min() {
            let mut dom = BitSetDom::from_values(vec![10, 11, 13, 15]);
            assert_dom(&dom, &[10, 11, 13, 15]);

            assert_eq!(dom.adjust_min(5), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[10, 11, 13, 15]);

            assert_eq!(dom.adjust_min(10), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[10, 11, 13, 15]);

            assert_eq!(dom.adjust_min(11), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[11, 13, 15]);

            assert_eq!(dom.adjust_min(12), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[13, 15]);

            assert_eq!(dom.adjust_min(15), Ok(ModEvent::Assigned));
            assert_dom(&dom, &[15]);

            assert_eq!(dom.adjust_min(20), Err(Error::DomEmpty));
            assert_dom(&dom, &[]);
        }

        #[test]
        fn adjust_max() {
            let mut dom = BitSetDom::from_values(vec![10, 12, 14, 15]);
            assert_dom(&dom, &[10, 12, 14, 15]);

            assert_eq!(dom.adjust_max(20), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[10, 12, 14, 15]);

            assert_eq!(dom.adjust_max(15), Ok(ModEvent::Unchanged));
            assert_dom(&dom, &[10, 12, 14, 15]);

            assert_eq!(dom.adjust_max(14), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[10, 12, 14]);

            assert_eq!(dom.adjust_max(13), Ok(ModEvent::BoundsChanged));
            assert_dom(&dom, &[10, 12]);

            assert_eq!(dom.adjust_max(10), Ok(ModEvent::Assigned));
            assert_dom(&dom, &[10]);

            assert_eq!(dom.adjust_max(5), Err(Error::DomEmpty));
            assert_dom(&dom, &[]);
        }

        // #[test]
        // fn intersect() {
        //     let mut dom = BitSetDom::from_bounds(0, 9);
        //     assert_eq!(
        //         dom.intersect(vec![-1, 1, 3, 5, 6, 13]),
        //         Ok(ModEvent::BoundsChanged)
        //     );
        //     assert_dom(&dom, &[1, 3, 5, 6]);

        //     assert_eq!(dom.intersect(vec![1, 3, 5, 6]), Ok(ModEvent::Unchanged));
        //     assert_dom(&dom, &[1, 3, 5, 6]);

        //     assert_eq!(dom.intersect(vec![1, 5, 6]), Ok(ModEvent::Reduced));
        //     assert_dom(&dom, &[1, 5, 6]);

        //     assert_eq!(dom.intersect(vec![5]), Ok(ModEvent::Assigned));
        //     assert_dom(&dom, &[5]);

        //     assert_eq!(dom.intersect(vec![5]), Ok(ModEvent::Unchanged));
        //     assert_dom(&dom, &[5]);

        //     assert_eq!(dom.intersect(vec![6]), Err(Error::DomEmpty));
        //     assert_dom(&dom, &[]);
        // }
    }
}
