//! Views.

use num_integer::Integer;

use std::marker::PhantomData;

use crate::{
    types::{Error, Int, Value},
    var::{self, Domain, ModEvent, View},
};

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct BaseView<D>
where
    D: Domain,
{
    phantom: PhantomData<D>,
}

impl<D> Clone for BaseView<D>
where
    D: Domain,
{
    fn clone(&self) -> Self {
        Self {
            phantom: PhantomData,
        }
    }
}

impl<D> Copy for BaseView<D> where D: Domain {}

impl<D> BaseView<D>
where
    D: Domain,
{
    pub fn new() -> Self {
        Self {
            phantom: PhantomData,
        }
    }
}

impl<D> View for BaseView<D>
where
    D: Domain,
{
    type Dom = D;
    type Value = Int;

    fn values<'s, 'd>(&'s self, dom: &'d D) -> Box<dyn DoubleEndedIterator<Item = Int> + 'd>
    where
        's: 'd,
    {
        Box::new(dom.values())
    }

    fn contains(&self, dom: &D, value: Int) -> bool {
        dom.contains(value)
    }

    fn value(&self, dom: &D) -> Option<Int> {
        dom.value()
    }

    fn random(&self, dom: &D) -> Int {
        dom.random()
    }

    fn min(&self, dom: &D) -> Int {
        dom.min()
    }

    fn max(&self, dom: &D) -> Int {
        dom.max()
    }

    fn assign(&self, dom: &mut D, value: Int) -> Result<ModEvent, var::Error> {
        dom.assign(value)
    }

    fn remove(&self, dom: &mut D, value: Int) -> Result<ModEvent, var::Error> {
        dom.remove(value)
    }

    fn adjust_min(&self, dom: &mut D, new_min: Int) -> Result<ModEvent, var::Error> {
        dom.adjust_min(new_min)
    }

    fn adjust_max(&self, dom: &mut D, new_max: Int) -> Result<ModEvent, var::Error> {
        dom.adjust_max(new_max)
    }

    fn intersect<I>(&self, dom: &mut D, values: I) -> Result<ModEvent, var::Error>
    where
        I: IntoIterator<Item = Int>,
        I::IntoIter: DoubleEndedIterator,
    {
        dom.intersect(values)
    }
}

/// View representing a constant value `c`.
#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct ConstantView<D, T>
where
    D: Domain,
    T: Value,
{
    value: T,
    phantom: PhantomData<D>,
}

impl<D, T> ConstantView<D, T>
where
    D: Domain,
    T: Value,
{
    pub fn new(value: T) -> Self {
        Self {
            value,
            phantom: PhantomData,
        }
    }
}

impl<D, T> Clone for ConstantView<D, T>
where
    D: Domain,
    T: Value,
{
    fn clone(&self) -> Self {
        Self {
            value: self.value,
            phantom: PhantomData,
        }
    }
}

impl<D, T> Copy for ConstantView<D, T>
where
    D: Domain,
    T: Value,
{
}

impl<D, T> View for ConstantView<D, T>
where
    D: Domain,
    T: Value,
{
    type Dom = D;
    type Value = T;

    fn values<'s, 'd>(&'s self, _dom: &'d D) -> Box<dyn DoubleEndedIterator<Item = T> + 'd>
    where
        's: 'd,
    {
        Box::new(Some(self.value).into_iter())
    }

    fn contains(&self, _dom: &D, value: T) -> bool {
        value == self.value
    }

    fn value(&self, _dom: &D) -> Option<T> {
        Some(self.value)
    }

    fn random(&self, _dom: &D) -> T {
        self.value
    }

    fn min(&self, _dom: &D) -> T {
        self.value
    }

    fn max(&self, _dom: &D) -> T {
        self.value
    }

    fn assign(&self, _dom: &mut D, value: T) -> Result<ModEvent, var::Error> {
        if value != self.value {
            Err(Error::DomEmpty)
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn remove(&self, _dom: &mut D, value: T) -> Result<ModEvent, var::Error> {
        if value == self.value {
            Err(Error::DomEmpty)
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_min(&self, _dom: &mut D, new_min: T) -> Result<ModEvent, var::Error> {
        if self.value < new_min {
            Err(Error::DomEmpty)
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_max(&self, _dom: &mut D, new_max: T) -> Result<ModEvent, var::Error> {
        if new_max < self.value {
            Err(Error::DomEmpty)
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn intersect<I>(&self, _dom: &mut D, values: I) -> Result<ModEvent, var::Error>
    where
        I: IntoIterator<Item = T>,
        I::IntoIter: DoubleEndedIterator,
    {
        if values.into_iter().find(|&v| v == self.value).is_some() {
            Ok(ModEvent::Unchanged)
        } else {
            Err(Error::DomEmpty)
        }
    }
}

/// Integer view on `x` representing `x + b`.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct OffsetView<V>
where
    V: View,
{
    inner: V,
    offset: V::Value,
}

impl<V> OffsetView<V>
where
    V: View<Value = Int>,
{
    pub fn new(inner: V, offset: Int) -> Self {
        Self { inner, offset }
    }
}

impl<V> View for OffsetView<V>
where
    V: View<Value = Int>,
{
    type Dom = V::Dom;
    type Value = V::Value;

    fn values<'s, 'd>(&'s self, dom: &'d V::Dom) -> Box<dyn DoubleEndedIterator<Item = Int> + 'd>
    where
        's: 'd,
    {
        Box::new(self.inner.values(dom).map(move |value| value + self.offset))
    }

    fn contains(&self, dom: &V::Dom, value: Int) -> bool {
        self.inner.contains(dom, value - self.offset)
    }

    fn value(&self, dom: &V::Dom) -> Option<Int> {
        self.inner.value(dom).map(|value| value + self.offset)
    }

    fn random(&self, dom: &V::Dom) -> Int {
        self.inner.random(dom) + self.offset
    }

    fn min(&self, dom: &V::Dom) -> Int {
        self.inner.min(dom) + self.offset
    }

    fn max(&self, dom: &V::Dom) -> Int {
        self.inner.max(dom) + self.offset
    }

    fn assign(&self, dom: &mut V::Dom, value: Int) -> Result<ModEvent, var::Error> {
        self.inner.assign(dom, value - self.offset)
    }

    fn remove(&self, dom: &mut V::Dom, value: Int) -> Result<ModEvent, var::Error> {
        self.inner.remove(dom, value - self.offset)
    }

    fn adjust_min(&self, dom: &mut V::Dom, new_min: Int) -> Result<ModEvent, var::Error> {
        self.inner.adjust_min(dom, new_min - self.offset)
    }

    fn adjust_max(&self, dom: &mut V::Dom, new_max: Int) -> Result<ModEvent, var::Error> {
        self.inner.adjust_max(dom, new_max - self.offset)
    }

    fn intersect<I>(&self, dom: &mut V::Dom, values: I) -> Result<ModEvent, var::Error>
    where
        I: IntoIterator<Item = Int>,
        I::IntoIter: DoubleEndedIterator,
    {
        self.inner
            .intersect(dom, values.into_iter().map(|v| v - self.offset))
    }
}

/// Integer view on `x` representing `a * x`, `a > 0`.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct ScaleView<V>
where
    V: View,
{
    inner: V,
    scale: V::Value,
}

impl<V> ScaleView<V>
where
    V: View<Value = Int>,
{
    pub fn new(inner: V, scale: Int) -> Self {
        assert!(scale > 0);
        Self { inner, scale }
    }
}

impl<V> View for ScaleView<V>
where
    V: View<Value = Int>,
{
    type Dom = V::Dom;
    type Value = V::Value;

    fn values<'s, 'd>(&'s self, dom: &'d V::Dom) -> Box<dyn DoubleEndedIterator<Item = Int> + 'd>
    where
        's: 'd,
    {
        Box::new(dom.values().map(move |value| value * self.scale))
    }

    fn contains(&self, dom: &V::Dom, value: Int) -> bool {
        if value.is_multiple_of(&self.scale) {
            self.inner.contains(dom, value / self.scale)
        } else {
            false
        }
    }

    fn value(&self, dom: &V::Dom) -> Option<Int> {
        self.inner.value(dom).map(|value| value * self.scale)
    }

    fn random(&self, dom: &V::Dom) -> Int {
        self.inner.random(dom) * self.scale
    }

    fn min(&self, dom: &V::Dom) -> Int {
        self.inner.min(dom) * self.scale
    }

    fn max(&self, dom: &V::Dom) -> Int {
        self.inner.max(dom) * self.scale
    }

    fn assign(&self, dom: &mut V::Dom, value: Self::Value) -> Result<ModEvent, var::Error> {
        if value.is_multiple_of(&self.scale) {
            self.inner.assign(dom, value / self.scale)
        } else {
            Err(var::Error::DomEmpty)
        }
    }

    fn remove(&self, dom: &mut V::Dom, value: Self::Value) -> Result<ModEvent, var::Error> {
        if value.is_multiple_of(&self.scale) {
            self.inner.remove(dom, value / self.scale)
        } else {
            Ok(ModEvent::Unchanged)
        }
    }

    fn adjust_min(&self, dom: &mut V::Dom, new_min: Int) -> Result<ModEvent, var::Error> {
        self.inner
            .adjust_min(dom, (new_min - 1).div_floor(&self.scale) + 1)
    }

    fn adjust_max(&self, dom: &mut V::Dom, new_max: Int) -> Result<ModEvent, var::Error> {
        self.inner.adjust_max(dom, new_max.div_floor(&self.scale))
    }

    fn intersect<I>(&self, dom: &mut V::Dom, values: I) -> Result<ModEvent, var::Error>
    where
        I: IntoIterator<Item = Int>,
        I::IntoIter: DoubleEndedIterator,
    {
        self.inner
            .intersect(dom, values.into_iter().map(|v| v * self.scale))
    }
}

/// Integer view on `x` representing `-x`.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct MinusView<V>
where
    V: View,
{
    inner: V,
}

impl<V> MinusView<V>
where
    V: View<Value = Int>,
{
    pub fn new(inner: V) -> Self {
        Self { inner }
    }
}

impl<V> View for MinusView<V>
where
    V: View<Value = Int>,
{
    type Dom = V::Dom;
    type Value = V::Value;

    fn values<'s, 'd>(&'s self, dom: &'d V::Dom) -> Box<DoubleEndedIterator<Item = Int> + 'd>
    where
        's: 'd,
    {
        Box::new(dom.values().map(|value| -value).rev())
    }

    fn contains(&self, dom: &V::Dom, value: Int) -> bool {
        self.inner.contains(dom, -value)
    }

    fn value(&self, dom: &V::Dom) -> Option<Int> {
        self.inner.value(dom).map(|value| -value)
    }

    fn random(&self, dom: &V::Dom) -> Int {
        -self.inner.random(dom)
    }

    fn min(&self, dom: &V::Dom) -> Int {
        -self.inner.max(dom)
    }

    fn max(&self, dom: &V::Dom) -> Int {
        -self.inner.min(dom)
    }

    fn assign(&self, dom: &mut V::Dom, value: Int) -> Result<ModEvent, var::Error> {
        self.inner.assign(dom, -value)
    }

    fn remove(&self, dom: &mut V::Dom, value: Int) -> Result<ModEvent, var::Error> {
        self.inner.remove(dom, -value)
    }

    fn adjust_min(&self, dom: &mut V::Dom, new_min: Self::Value) -> Result<ModEvent, var::Error> {
        self.inner.adjust_max(dom, -new_min)
    }

    fn adjust_max(&self, dom: &mut V::Dom, new_max: Self::Value) -> Result<ModEvent, var::Error> {
        self.inner.adjust_min(dom, -new_max)
    }

    fn intersect<I>(&self, dom: &mut V::Dom, values: I) -> Result<ModEvent, var::Error>
    where
        I: IntoIterator<Item = Int>,
        I::IntoIter: DoubleEndedIterator,
    {
        self.inner
            .intersect(dom, values.into_iter().map(|v| -v).rev())
    }
}
