use std::marker::PhantomData;

use crate::{
    search::{Decision, State},
    var,
};

pub trait Branch {
    type Dom: var::Domain;

    fn branch(
        &self,
        state: &State<Self::Dom>,
        vars: &[var::IntVar<Self::Dom>],
    ) -> (
        Decision<var::views::BaseView<Self::Dom>>,
        Decision<var::views::BaseView<Self::Dom>>,
    );
}

#[derive(Clone, Copy, Debug)]
pub enum VarStrategy {
    InOrder,
    FailFirst,
}

#[derive(Clone, Copy, Debug)]
pub enum ValStrategy {
    Minimum,
    Maximum,
    Random,
}

#[derive(Clone)]
pub struct VarValBrancher<D>
where
    D: var::Domain,
{
    var_strat: VarStrategy,
    val_strat: ValStrategy,
    phantom: PhantomData<D>,
}

impl<D> VarValBrancher<D>
where
    D: var::Domain,
{
    pub fn new(var_strat: VarStrategy, val_strat: ValStrategy) -> Self {
        VarValBrancher {
            var_strat,
            val_strat,
            phantom: PhantomData,
        }
    }
}

impl<D> Branch for VarValBrancher<D>
where
    D: var::Domain,
{
    type Dom = D;

    fn branch(
        &self,
        state: &State<D>,
        vars: &[var::IntVar<Self::Dom>],
    ) -> (
        Decision<var::views::BaseView<D>>,
        Decision<var::views::BaseView<D>>,
    ) {
        // unwraps are safe because state is not solved
        let x = match self.var_strat {
            VarStrategy::InOrder => vars
                .iter()
                .filter(|&&x| !state.vars.is_assigned(x))
                .next()
                .unwrap()
                .clone(),
            VarStrategy::FailFirst => vars
                .iter()
                .filter(|&&x| !state.vars.is_assigned(x))
                .min_by_key(|&&x| state.vars.size(x))
                .unwrap()
                .clone(),
        };

        // safe to call these methods because x is unassigned
        let val = match self.val_strat {
            ValStrategy::Minimum => state.vars.min(x),
            ValStrategy::Maximum => state.vars.max(x),
            ValStrategy::Random => state.vars.random(x),
        };

        return (Decision::Eq(x, val), Decision::Neq(x, val));
    }
}
