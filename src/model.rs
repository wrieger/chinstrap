//! Model

pub mod array;
pub mod builders;
pub mod matrix;

pub use self::{array::Array, matrix::Matrix};

use std::{
    borrow::Borrow,
    cell::RefCell,
    collections::BTreeSet,
    ops::{Range, RangeInclusive},
    rc::Rc,
};

use self::builders::{
    AbsBuilder, AllDiffBuilder, ArithBuilder, ArithLtBuilder, ArithNeBuilder, CountBuilder,
    RelBuilder, SumEqBuilder, SumLeqBuilder, SumNeqBuilder,
};

use crate::{
    linexpr::{LinExpr, TryIntoVarResult},
    prop,
    search::{
        branch::{ValStrategy, VarStrategy, VarValBrancher},
        Minimize, Solve, State,
    },
    types::{Int, RelOp, Value},
    var::{self, IntVar, Var, View},
};

pub struct Search<D>
where
    D: var::Domain,
{
    branch_vars: Option<Vec<IntVar<D>>>,
    var_strat: VarStrategy,
    val_strat: ValStrategy,
}

pub struct SearchBuilder<D>
where
    D: var::Domain,
{
    branch_vars: Option<Vec<IntVar<D>>>,
    var_strat: Option<VarStrategy>,
    val_strat: Option<ValStrategy>,
}

impl<D> SearchBuilder<D>
where
    D: var::Domain,
{
    pub fn new() -> Self {
        Self {
            branch_vars: None,
            var_strat: None,
            val_strat: None,
        }
    }

    pub fn branch_on<B, I>(mut self, vars: I) -> Self
    where
        B: Borrow<IntVar<D>>,
        I: IntoIterator<Item = B>,
    {
        self.branch_vars = Some(vars.into_iter().map(|b| *b.borrow()).collect());
        self
    }

    pub fn choose_var_by(mut self, var_strat: VarStrategy) -> Self {
        self.var_strat = Some(var_strat);
        self
    }

    pub fn choose_val_by(mut self, val_strat: ValStrategy) -> Self {
        self.val_strat = Some(val_strat);
        self
    }

    pub fn finish(self) -> Search<D> {
        Search {
            branch_vars: self.branch_vars,
            var_strat: self.var_strat.unwrap_or(VarStrategy::FailFirst),
            val_strat: self.val_strat.unwrap_or(ValStrategy::Minimum),
        }
    }
}

#[derive(Clone)]
enum IntDom {
    Range { min: Int, max: Int },
    Values { values: BTreeSet<Int> },
}

pub struct Model<D>
where
    D: var::Domain,
{
    state: Rc<RefCell<State<D>>>,
}

impl<D> Model<D>
where
    D: var::Domain + 'static,
{
    /// Creates a new model with no variables.
    pub fn new() -> Self {
        Self {
            state: Rc::new(RefCell::new(State::new())),
        }
    }

    /// Entry point to create integer variables.
    pub fn int_var(&mut self) -> IntVarBuilder<D> {
        IntVarBuilder {
            state: self.state.clone(),
            dom: None,
            expr: None,
        }
    }

    /// Creates the constraint `x == y`.
    pub fn eq<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Eq, y.into())
    }

    /// Creates the constraint `x != y`.
    pub fn ne<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Ne, y.into())
    }

    /// Creates the constraint `x > y`.
    pub fn gt<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Ge, y.into())
    }

    /// Creates the constraint `x >= y`.
    pub fn ge<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Gt, y.into())
    }

    /// Creates the constraint `x < y`.
    pub fn lt<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Lt, y.into())
    }

    /// Creates the constraint `x <= y`.
    pub fn le<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> RelBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        RelBuilder::new(self.state.clone(), x.into(), RelOp::Lt, y.into())
    }

    /// Creates the constraint `|x| == y`.
    pub fn abs<Vx, Vy, X, Y>(&mut self, x: X, y: Y) -> AbsBuilder<Vx, Vy>
    where
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        AbsBuilder::new(self.state.clone(), x.into(), y.into())
    }

    pub fn sum_eq<Bx, Ix, Vx, Vy, Y>(&mut self, x: Ix, y: Y) -> SumEqBuilder<Vx, Vy>
    where
        Bx: Borrow<Var<Vx>>,
        Ix: IntoIterator<Item = Bx>,
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        Y: Into<Var<Vy>>,
    {
        SumEqBuilder::new(
            self.state.clone(),
            x.into_iter().map(|b| *b.borrow()).collect(),
            y.into(),
        )
    }

    pub fn sum_neq<Bx, Ix, Vx, Vy, Y>(&mut self, x: Ix, y: Y) -> SumNeqBuilder<Vx, Vy>
    where
        Bx: Borrow<Var<Vx>>,
        Ix: IntoIterator<Item = Bx>,
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        Y: Into<Var<Vy>>,
    {
        SumNeqBuilder::new(
            self.state.clone(),
            x.into_iter().map(|b| *b.borrow()).collect(),
            y.into(),
        )
    }

    pub fn sum_leq<Bx, Ix, Vx, Vy, Y>(&mut self, x: Ix, y: Y) -> SumLeqBuilder<Vx, Vy>
    where
        Bx: Borrow<Var<Vx>>,
        Ix: IntoIterator<Item = Bx>,
        Vx: View<Dom = D, Value = Int> + 'static,
        Vy: View<Dom = D, Value = Int> + 'static,
        Y: Into<Var<Vy>>,
    {
        SumLeqBuilder::new(
            self.state.clone(),
            x.into_iter().map(|b| *b.borrow()).collect(),
            y.into(),
        )
    }

    pub fn count<Bx, Ix, T, Vx, Vy, Vz, Y, Z>(
        &mut self,
        vars: Ix,
        value: Y,
        count: Z,
    ) -> CountBuilder<Vx, Vy, Vz>
    where
        Bx: Borrow<Var<Vx>>,
        Ix: IntoIterator<Item = Bx>,
        T: Value,
        Vx: View<Dom = D, Value = T> + 'static,
        Vy: View<Dom = D, Value = T> + 'static,
        Vz: View<Dom = D, Value = Int> + 'static,
        Y: Into<Var<Vy>>,
        Z: Into<Var<Vz>>,
    {
        CountBuilder::new(
            self.state.clone(),
            vars.into_iter().map(|b| *b.borrow()).collect(),
            value.into(),
            count.into(),
        )
    }

    pub fn all_diff<B, I, V>(&mut self, vars: I) -> AllDiffBuilder<V>
    where
        B: Borrow<Var<V>>,
        I: IntoIterator<Item = B>,
        V: View<Dom = D, Value = Int> + 'static,
    {
        let vars = vars.into_iter().map(|b| *b.borrow()).collect::<Vec<_>>();
        AllDiffBuilder::new(self.state.clone(), vars, false)
    }

    pub fn arith<L, R>(&mut self, lhs: L, rhs: R) -> ArithBuilder<D>
    where
        L: Into<LinExpr<D>>,
        R: Into<LinExpr<D>>,
    {
        ArithBuilder::new(self.state.clone(), lhs.into(), rhs.into())
    }

    pub fn arith_ne<L, R>(&mut self, lhs: L, rhs: R) -> ArithNeBuilder<D>
    where
        L: Into<LinExpr<D>>,
        R: Into<LinExpr<D>>,
    {
        ArithNeBuilder::new(self.state.clone(), lhs.into(), rhs.into())
    }

    pub fn arith_lt<L, R>(&mut self, lhs: L, rhs: R) -> ArithLtBuilder<D>
    where
        L: Into<LinExpr<D>>,
        R: Into<LinExpr<D>>,
    {
        ArithLtBuilder::new(self.state.clone(), lhs.into(), rhs.into())
    }

    pub fn solve(self, search: Search<D>) -> Solve<VarValBrancher<D>, D> {
        let state = Rc::try_unwrap(self.state).expect("bad model").into_inner();
        let default_branch_vars = state.doms().vars().collect();
        state.solve(
            search.branch_vars.unwrap_or(default_branch_vars),
            search.var_strat,
            search.val_strat,
        )
    }

    pub fn minimize<V>(
        self,
        search: Search<D>,
        var: var::Var<V>,
    ) -> Minimize<VarValBrancher<D>, D, V>
    where
        V: var::View<Dom = D, Value = Int>,
    {
        let state = Rc::try_unwrap(self.state).expect("bad model").into_inner();
        let default_branch_vars = state.doms().vars().collect();
        state.minimize(
            var,
            search.branch_vars.unwrap_or(default_branch_vars),
            search.var_strat,
            search.val_strat,
        )
    }

    pub fn maximize<V>(
        self,
        search: Search<D>,
        var: var::Var<V>,
    ) -> Minimize<VarValBrancher<D>, D, var::views::MinusView<V>>
    where
        V: var::View<Dom = D, Value = Int>,
    {
        self.minimize(search, var.minus())
    }
}

pub struct IntVarBuilder<D>
where
    D: var::Domain,
{
    state: Rc<RefCell<State<D>>>,
    dom: Option<IntDom>,
    expr: Option<LinExpr<D>>,
}

impl<D> IntVarBuilder<D>
where
    D: var::Domain + 'static,
{
    fn add_var(&mut self) -> IntVar<D> {
        match self.expr.clone().map(|expr| expr.try_into_var()) {
            Some(res) => match res {
                TryIntoVarResult::Const(c) => {
                    let dom = match self.dom {
                        Some(IntDom::Range { min, max }) => D::from_bounds(min, max),
                        Some(IntDom::Values { ref values }) => D::from_values(values.clone()),
                        None => {
                            let c_val = RefCell::borrow(&self.state).doms().value(c).unwrap();
                            D::from_bounds(c_val, c_val)
                        }
                    };
                    let x = self.state.borrow_mut().add_dom(dom);
                    self.state
                        .borrow_mut()
                        .add_prop(prop::rel::make_prop(x, RelOp::Eq, c));
                    x
                }
                TryIntoVarResult::PosExpr(affine) => {
                    let dom = match self.dom {
                        Some(IntDom::Range { min, max }) => D::from_bounds(min, max),
                        Some(IntDom::Values { ref values }) => D::from_values(values.clone()),
                        None => {
                            let min = RefCell::borrow(&self.state).doms().min(affine);
                            let max = RefCell::borrow(&self.state).doms().max(affine);
                            D::from_bounds(min, max)
                        }
                    };
                    let x = self.state.borrow_mut().add_dom(dom);
                    self.state
                        .borrow_mut()
                        .add_prop(prop::rel::make_prop(x, RelOp::Eq, affine));
                    x
                }
                TryIntoVarResult::NegExpr(affine) => {
                    let dom = match self.dom {
                        Some(IntDom::Range { min, max }) => D::from_bounds(min, max),
                        Some(IntDom::Values { ref values }) => D::from_values(values.clone()),
                        None => {
                            let min = RefCell::borrow(&self.state).doms().min(affine);
                            let max = RefCell::borrow(&self.state).doms().max(affine);
                            D::from_bounds(min, max)
                        }
                    };
                    let x = self.state.borrow_mut().add_dom(dom);
                    self.state
                        .borrow_mut()
                        .add_prop(prop::rel::make_prop(x, RelOp::Eq, affine));
                    x
                }
                TryIntoVarResult::CannotIntoVar(expr) => {
                    let dom = match self.dom {
                        Some(IntDom::Range { min, max }) => D::from_bounds(min, max),
                        Some(IntDom::Values { ref values }) => D::from_values(values.clone()),
                        None => {
                            let mut min = 0;
                            let mut max = 0;
                            let borrow = RefCell::borrow(&self.state);
                            for (x, coeff) in expr.vars() {
                                if coeff > 0 {
                                    min += coeff * borrow.doms().min(x);
                                    max += coeff * borrow.doms().max(x);
                                } else if coeff < 0 {
                                    min += coeff * borrow.doms().max(x);
                                    max += coeff * borrow.doms().min(x);
                                }
                            }
                            D::from_bounds(min, max)
                        }
                    };
                    let var = self.state.borrow_mut().add_dom(dom);
                    let equals_zero = var - expr;
                    let mut pos_vars = vec![];
                    let mut neg_vars = vec![];
                    for (x, coeff) in equals_zero.vars() {
                        if coeff > 0 {
                            pos_vars.push(x.scale(coeff));
                        } else if coeff < 0 {
                            neg_vars.push(x.scale(-coeff));
                        } else {
                            unreachable!()
                        }
                    }
                    self.state
                        .borrow_mut()
                        .add_prop(Box::new(prop::sum::SumProp::new(pos_vars, neg_vars, 0)));
                    var
                }
            },
            None => {
                let dom = match self.dom {
                    Some(IntDom::Range { min, max }) => D::from_bounds(min, max),
                    Some(IntDom::Values { ref values }) => D::from_values(values.clone()),
                    None => unimplemented!(),
                };
                self.state.borrow_mut().add_dom(dom)
            }
        }
    }

    pub fn values<B, I>(mut self, values: I) -> Self
    where
        B: Borrow<Int>,
        I: IntoIterator<Item = B>,
    {
        self.dom = Some(IntDom::Values {
            values: values.into_iter().map(|b| *b.borrow()).collect(),
        });
        self
    }

    pub fn expr<I>(mut self, expr: I) -> Self
    where
        I: Into<LinExpr<D>>,
    {
        self.expr = Some(expr.into());
        self
    }

    pub fn range<R>(mut self, range: R) -> Self
    where
        R: Into<IntDomRange>,
    {
        let range = range.into();
        self.dom = Some(IntDom::Range {
            min: range.min,
            max: range.max,
        });
        self
    }

    pub fn assigned(mut self, val: Int) -> Self {
        self.dom = Some(IntDom::Range { min: val, max: val });
        self
    }

    pub fn create(mut self) -> IntVar<D> {
        self.add_var()
    }

    pub fn create_array(mut self, len: usize) -> Array<IntVar<D>> {
        (0..len).map(|_| self.add_var()).collect()
    }

    pub fn create_matrix(mut self, rows: usize, cols: usize) -> Matrix<IntVar<D>> {
        (0..rows)
            .map(|_| (0..cols).map(|_| self.add_var()).collect())
            .collect()
    }
}

#[derive(Clone, Copy, Debug)]
pub struct IntDomRange {
    min: Int,
    max: Int,
}

impl From<Range<Int>> for IntDomRange {
    fn from(range: Range<Int>) -> IntDomRange {
        IntDomRange {
            min: range.start,
            max: range.end - 1,
        }
    }
}

impl From<RangeInclusive<Int>> for IntDomRange {
    fn from(range: RangeInclusive<Int>) -> IntDomRange {
        IntDomRange {
            min: *range.start(),
            max: *range.end(),
        }
    }
}
