use crate::{
    prop::{self, Propagate},
    types::{Error, Int, RelOp},
    var::{self, Domain, Var, View},
};

/// Propagator for `Σ xs - Σ ys = c`.
#[derive(Clone, Debug)]
pub struct SumProp<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    xs: Vec<Var<Vx>>,
    ys: Vec<Var<Vy>>,
    c: Int,
}

impl<D, Vx, Vy> SumProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int>,
    Vy: View<Dom = D, Value = Int>,
{
    pub fn new(xs: Vec<Var<Vx>>, ys: Vec<Var<Vy>>, c: Int) -> Self {
        Self { xs, ys, c }
    }
}

impl<D, Vx, Vy> Propagate for SumProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Linear
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        let mut to_remove = Vec::with_capacity(Ord::max(self.xs.len(), self.ys.len()));
        for (i, &x) in self.xs.iter().enumerate() {
            if let Some(val) = ctx.value(x) {
                self.c -= val;
                to_remove.push(i);
            }
        }
        for &i in to_remove.iter().rev() {
            self.xs.swap_remove(i);
        }

        to_remove.clear();
        for (i, &y) in self.ys.iter().enumerate() {
            if let Some(val) = ctx.value(y) {
                self.c += val;
                to_remove.push(i);
            }
        }
        for &i in to_remove.iter().rev() {
            self.ys.swap_remove(i);
        }

        let xs_len = self.xs.len();
        let ys_len = self.ys.len();
        if xs_len + ys_len == 0 {
            if self.c == 0 {
                Ok(prop::Status::Entailed)
            } else {
                Err(Error::PropUnentailed)
            }
        } else if xs_len + ys_len == 1 {
            if xs_len == 1 {
                ctx.assign(self.xs[0], self.c)?;
                Ok(prop::Status::Entailed)
            } else {
                ctx.assign(self.ys[0], -self.c)?;
                Ok(prop::Status::Entailed)
            }
        } else if xs_len + ys_len == 2 {
            if xs_len == 2 {
                let x0 = self.xs[0];
                let x1 = self.xs[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Eq,
                    x1.minus().offset(self.c),
                )))
            } else if xs_len == 1 {
                let x0 = self.xs[0];
                let y0 = self.ys[0];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Eq,
                    y0.offset(self.c),
                )))
            } else {
                let y0 = self.ys[0];
                let y1 = self.ys[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    y0,
                    RelOp::Eq,
                    y1.minus().offset(-self.c),
                )))
            }
        } else {
            if first_time {
                for &x in &self.xs {
                    ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                }
                for &y in &self.ys {
                    ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
                }
            }

            let (x_tot_min, x_tot_max) = self
                .xs
                .iter()
                .map(|&x| (ctx.min(x), ctx.max(x)))
                .fold((0, 0), |(tot_min, tot_max), (min, max)| {
                    (tot_min + min, tot_max + max)
                });
            let (y_tot_min, y_tot_max) = self
                .ys
                .iter()
                .map(|&y| (ctx.min(y), ctx.max(y)))
                .fold((0, 0), |(tot_min, tot_max), (min, max)| {
                    (tot_min + min, tot_max + max)
                });
            for &x in &self.xs {
                ctx.adjust_min(x, self.c + y_tot_min - x_tot_max + ctx.max(x))?;
                ctx.adjust_max(x, self.c + y_tot_max - x_tot_min + ctx.min(x))?;
            }
            for &y in &self.ys {
                ctx.adjust_min(y, -self.c + x_tot_min - y_tot_max + ctx.max(y))?;
                ctx.adjust_max(y, -self.c + x_tot_max - y_tot_min + ctx.min(y))?;
            }

            Ok(prop::Status::Unsure)
        }
    }
}

/// Propagator for `Σ xs - Σ ys != c`.
#[derive(Clone, Debug)]
pub struct SumNeqProp<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    xs: Vec<Var<Vx>>,
    ys: Vec<Var<Vy>>,
    c: Int,
}

impl<D, Vx, Vy> SumNeqProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int>,
    Vy: View<Dom = D, Value = Int>,
{
    pub fn new(xs: Vec<Var<Vx>>, ys: Vec<Var<Vy>>, c: Int) -> Self {
        Self { xs, ys, c }
    }
}

impl<D, Vx, Vy> Propagate for SumNeqProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Linear
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        let mut to_remove = Vec::with_capacity(Ord::max(self.xs.len(), self.ys.len()));
        for (i, &x) in self.xs.iter().enumerate() {
            if let Some(val) = ctx.value(x) {
                self.c -= val;
                to_remove.push(i);
            }
        }
        for &i in to_remove.iter().rev() {
            self.xs.swap_remove(i);
        }

        to_remove.clear();
        for (i, &y) in self.ys.iter().enumerate() {
            if let Some(val) = ctx.value(y) {
                self.c += val;
                to_remove.push(i);
            }
        }
        for &i in to_remove.iter().rev() {
            self.ys.swap_remove(i);
        }

        let xs_len = self.xs.len();
        let ys_len = self.ys.len();
        if xs_len + ys_len == 0 {
            if self.c != 0 {
                Ok(prop::Status::Entailed)
            } else {
                Err(Error::PropUnentailed)
            }
        } else if xs_len + ys_len == 1 {
            if xs_len == 1 {
                ctx.remove(self.xs[0], self.c)?;
                Ok(prop::Status::Entailed)
            } else {
                ctx.remove(self.ys[0], -self.c)?;
                Ok(prop::Status::Entailed)
            }
        } else if xs_len + ys_len == 2 {
            if xs_len == 2 {
                let x0 = self.xs[0];
                let x1 = self.xs[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Ne,
                    x1.minus().offset(self.c),
                )))
            } else if xs_len == 1 {
                let x0 = self.xs[0];
                let y0 = self.ys[0];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Ne,
                    y0.offset(self.c),
                )))
            } else {
                let y0 = self.ys[0];
                let y1 = self.ys[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    y0,
                    RelOp::Ne,
                    y1.minus().offset(-self.c),
                )))
            }
        } else {
            if first_time {
                for &x in &self.xs {
                    ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                }
                for &y in &self.ys {
                    ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
                }
            }

            let (x_tot_min, x_tot_max) = self
                .xs
                .iter()
                .map(|&x| (ctx.min(x), ctx.max(x)))
                .fold((0, 0), |(tot_min, tot_max), (min, max)| {
                    (tot_min + min, tot_max + max)
                });
            let (y_tot_min, y_tot_max) = self
                .ys
                .iter()
                .map(|&y| (ctx.min(y), ctx.max(y)))
                .fold((0, 0), |(tot_min, tot_max), (min, max)| {
                    (tot_min + min, tot_max + max)
                });

            if x_tot_max - y_tot_min < self.c || x_tot_min - y_tot_max > self.c {
                Ok(prop::Status::Entailed)
            } else {
                Ok(prop::Status::Unsure)
            }
        }
    }
}

/// Propagator for `Σ xs - Σ ys <= c`.
#[derive(Clone, Debug)]
pub struct SumLeqProp<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    xs: Vec<Var<Vx>>,
    ys: Vec<Var<Vy>>,
    c: Int,
}

impl<D, Vx, Vy> SumLeqProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int>,
    Vy: View<Dom = D, Value = Int>,
{
    pub fn new(xs: Vec<Var<Vx>>, ys: Vec<Var<Vy>>, c: Int) -> Self {
        Self { xs, ys, c }
    }
}

impl<D, Vx, Vy> Propagate for SumLeqProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Linear
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        for i in (0..self.xs.len()).rev() {
            let x = self.xs[i];
            if let Some(val) = ctx.value(x) {
                self.c -= val;
                self.xs.swap_remove(i);
            }
        }

        for i in (0..self.ys.len()).rev() {
            let y = self.ys[i];
            if let Some(val) = ctx.value(y) {
                self.c += val;
                self.ys.swap_remove(i);
            }
        }

        let xs_len = self.xs.len();
        let ys_len = self.ys.len();
        if xs_len + ys_len == 0 {
            if 0 <= self.c {
                Ok(prop::Status::Entailed)
            } else {
                Err(Error::PropUnentailed)
            }
        } else if xs_len + ys_len == 1 {
            if xs_len == 1 {
                ctx.adjust_max(self.xs[0], self.c)?;
                Ok(prop::Status::Entailed)
            } else {
                ctx.adjust_min(self.ys[0], -self.c)?;
                Ok(prop::Status::Entailed)
            }
        } else if xs_len + ys_len == 2 {
            if xs_len == 2 {
                let x0 = self.xs[0];
                let x1 = self.xs[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Le,
                    x1.minus().offset(self.c),
                )))
            } else if xs_len == 1 {
                let x0 = self.xs[0];
                let y0 = self.ys[0];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    x0,
                    RelOp::Le,
                    y0.offset(self.c),
                )))
            } else {
                let y0 = self.ys[0];
                let y1 = self.ys[1];
                Ok(prop::Status::Rewrite(prop::rel::make_prop(
                    y0,
                    RelOp::Ge,
                    y1.minus().offset(-self.c),
                )))
            }
        } else {
            if first_time {
                for &x in &self.xs {
                    ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                }
                for &y in &self.ys {
                    ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
                }
            }

            let x_tot_min = self.xs.iter().map(|&x| ctx.min(x)).sum::<Int>();
            let y_tot_max = self.ys.iter().map(|&y| ctx.max(y)).sum::<Int>();
            for &x in &self.xs {
                ctx.adjust_max(x, self.c + y_tot_max - x_tot_min + ctx.min(x))?;
            }
            for &y in &self.ys {
                ctx.adjust_min(y, -self.c + x_tot_min - y_tot_max + ctx.max(y))?;
            }

            Ok(prop::Status::Unsure)
        }
    }
}
