use crate::{
    prop,
    types::{Error, Int, Value},
    var,
};

#[derive(Clone, Debug)]
pub struct CountEqProp<Vx, Vy, Vz>
where
    Vx: var::View,
    Vy: var::View,
    Vz: var::View,
{
    vars: Vec<var::Var<Vx>>,
    value: var::Var<Vy>,
    count: var::Var<Vz>,
}

impl<D, T, Vx, Vy, Vz> CountEqProp<Vx, Vy, Vz>
where
    D: var::Domain,
    T: Value,
    Vx: var::View<Dom = D, Value = T>,
    Vy: var::View<Dom = D, Value = T>,
    Vz: var::View<Dom = D, Value = Int>,
{
    pub fn new(vars: Vec<var::Var<Vx>>, value: var::Var<Vy>, count: var::Var<Vz>) -> Self {
        Self { vars, value, count }
    }
}

impl<D, T, Vx, Vy, Vz> prop::Propagate for CountEqProp<Vx, Vy, Vz>
where
    D: var::Domain,
    T: Value,
    Vx: var::View<Dom = D, Value = T> + 'static,
    Vy: var::View<Dom = D, Value = T> + 'static,
    Vz: var::View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn prop::Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Linear
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        if first_time {
            for &x in &self.vars {
                ctx.subscribe(x, var::ModTrigger::OnAssigned);
            }
            ctx.subscribe(self.value, var::ModTrigger::OnAssigned);
            ctx.subscribe(self.count, var::ModTrigger::OnAssigned);
        }

        if let Some(value) = ctx.value(self.value) {
            // let num_assigned_value = self
            //     .vars
            //     .iter()
            //     .filter(|&&x| ctx.value(x) == Some(value))
            //     .count();
            // let num_contains_value = self
            //     .vars
            //     .iter()
            //     .filter(|&&x| ctx.contains(x, value))
            //     .count();
            // ctx.adjust_min(self.count, num_assigned_value as Int)?;
            // ctx.adjust_max(self.count, num_contains_value as Int)?;

            // if let Some(count) = ctx.value(self.count) {
            //     if count == num_assigned_value as Int {
            //         for i in 0..self.vars.len() {
            //             if ctx.value(self.vars[i]) != Some(value) {
            //                 ctx.remove(self.vars[i], value)?;
            //             }
            //         }
            //         return Ok(prop::Status::Entailed);
            //     } else if count == num_contains_value as Int {
            //         for i in 0..self.vars.len() {
            //             if ctx.contains(self.vars[i], value) {
            //                 ctx.assign(self.vars[i], value)?;
            //             }
            //         }
            //         return Ok(prop::Status::Entailed);
            //     }
            // }
            // Ok(prop::Status::Unsure)
            Ok(prop::Status::Rewrite(Box::new(CountEqValueProp::new(
                self.vars.clone(),
                value,
                self.count,
            ))))
        } else {
            Ok(prop::Status::Unsure)
        }
    }
}

#[derive(Clone, Debug)]
pub struct CountEqValueProp<Vx, Vy>
where
    Vx: var::View,
    Vy: var::View,
{
    vars: Vec<var::Var<Vx>>,
    value: Vx::Value,
    count: var::Var<Vy>,
    num_assigned: Int,
}

impl<D, Vx, Vy> CountEqValueProp<Vx, Vy>
where
    D: var::Domain,
    Vx: var::View<Dom = D>,
    Vy: var::View<Dom = D, Value = Int>,
{
    pub fn new(vars: Vec<var::Var<Vx>>, value: Vx::Value, count: var::Var<Vy>) -> Self {
        Self {
            vars,
            value,
            count,
            num_assigned: 0,
        }
    }
}

impl<D, Vx, Vy> prop::Propagate for CountEqValueProp<Vx, Vy>
where
    D: var::Domain,
    Vx: var::View<Dom = D> + 'static,
    Vy: var::View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn prop::Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Linear
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        if first_time {
            for &x in &self.vars {
                ctx.subscribe(x, var::ModTrigger::OnAssigned);
            }
            ctx.subscribe(self.count, var::ModTrigger::OnAssigned);
        }

        for i in (0..self.vars.len()).rev() {
            let x = self.vars[i];
            if ctx.value(x) == Some(self.value) {
                self.vars.swap_remove(i);
                self.num_assigned += 1;
            } else if !ctx.contains(x, self.value) {
                self.vars.swap_remove(i);
            } else {
                continue;
            }
        }

        ctx.adjust_min(self.count, self.num_assigned)?;
        ctx.adjust_max(self.count, self.num_assigned + self.vars.len() as Int)?;

        if let Some(count) = ctx.value(self.count) {
            if count == self.num_assigned as Int {
                for &x in &self.vars {
                    ctx.remove(x, self.value)?;
                }
                return Ok(prop::Status::Entailed);
            } else if count == self.num_assigned + self.vars.len() as Int {
                for &x in &self.vars {
                    ctx.assign(x, self.value)?;
                }
                return Ok(prop::Status::Entailed);
            }
        }

        Ok(prop::Status::Unsure)
    }
}
