//! Propagators for boolean connectives.

use crate::{
    prop::{self, Propagate},
    var::{self, Domain, Var, View},
};

/// Propagator for `x == y`.
#[derive(Clone, Debug)]
pub struct EqProp<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> EqProp<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = bool>,
    Vy: View<Dom = D, Value = bool>,
{
    pub fn new<X, Y>(x: X, y: Y) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
        }
    }
}

impl<D, Vx, Vy> Propagate for EqProp<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = bool> + 'static,
    Vy: View<Dom = D, Value = bool> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Binary
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, prop::Error> {
        let x = self.x;
        let y = self.y;

        match (ctx.value(x), ctx.value(y)) {
            (Some(x_val), _) => {
                ctx.assign(y, x_val)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(y_val)) => {
                ctx.assign(x, y_val)?;
                Ok(prop::Status::Entailed)
            }

            (None, None) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                }
                Ok(prop::Status::Unsure)
            }
        }
    }
}

/// Propagator for `(x && y) == z`.
#[derive(Clone, Debug)]
pub struct AndProp<Vx, Vy, Vz>
where
    Vx: View,
    Vy: View,
    Vz: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
    z: Var<Vz>,
    cost: prop::Cost,
}

impl<D, Vx, Vy, Vz> AndProp<Vx, Vy, Vz>
where
    D: Domain,
    Vx: View<Dom = D, Value = bool>,
    Vy: View<Dom = D, Value = bool>,
    Vz: View<Dom = D, Value = bool>,
{
    pub fn new<X, Y, Z>(x: X, y: Y, z: Z) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
        Z: Into<Var<Vz>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
            z: z.into(),
            cost: prop::Cost::Ternary,
        }
    }
}

impl<D, Vx, Vy, Vz> Propagate for AndProp<Vx, Vy, Vz>
where
    D: Domain,
    Vx: View<Dom = D, Value = bool> + 'static,
    Vy: View<Dom = D, Value = bool> + 'static,
    Vz: View<Dom = D, Value = bool> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        self.cost
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, prop::Error> {
        let x = self.x;
        let y = self.y;
        let z = self.z;

        match (ctx.value(x), ctx.value(y), ctx.value(z)) {
            (Some(false), _, _) => {
                ctx.assign(z, false)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(false), _) => {
                ctx.assign(z, false)?;
                Ok(prop::Status::Entailed)
            }

            (_, _, Some(true)) => {
                ctx.assign(x, true)?;
                ctx.assign(y, true)?;
                Ok(prop::Status::Entailed)
            }

            (Some(true), Some(true), _) => {
                ctx.assign(z, true)?;
                Ok(prop::Status::Entailed)
            }

            (Some(true), _, Some(false)) => {
                ctx.assign(y, false)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(true), Some(false)) => {
                ctx.assign(x, false)?;
                Ok(prop::Status::Entailed)
            }

            (Some(_), None, None) => {
                if first_time {
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, Some(_), None) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, None, Some(_)) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, None, None) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Ternary;
                Ok(prop::Status::Unsure)
            }
        }
    }
}

/// Propagator for `(x == y) == z`.
#[derive(Clone, Debug)]
pub struct IffProp<Vx, Vy, Vz>
where
    Vx: View,
    Vy: View,
    Vz: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
    z: Var<Vz>,
    cost: prop::Cost,
}

impl<D, Vx, Vy, Vz> IffProp<Vx, Vy, Vz>
where
    D: Domain,
    Vx: View<Dom = D, Value = bool>,
    Vy: View<Dom = D, Value = bool>,
    Vz: View<Dom = D, Value = bool>,
{
    /// Creates a new `IffProp` from the given views.
    pub fn new<X, Y, Z>(x: X, y: Y, z: Z) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
        Z: Into<Var<Vz>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
            z: z.into(),
            cost: prop::Cost::Ternary,
        }
    }
}

impl<D, Vx, Vy, Vz> Propagate for IffProp<Vx, Vy, Vz>
where
    D: Domain,
    Vx: View<Dom = D, Value = bool> + 'static,
    Vy: View<Dom = D, Value = bool> + 'static,
    Vz: View<Dom = D, Value = bool> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Ternary
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, prop::Error> {
        let x = self.x;
        let y = self.y;
        let z = self.z;

        match (ctx.value(x), ctx.value(y), ctx.value(z)) {
            (Some(true), Some(true), _) | (Some(false), Some(false), _) => {
                ctx.assign(z, true)?;
                Ok(prop::Status::Entailed)
            }

            (Some(true), Some(false), _) | (Some(false), Some(true), _) => {
                ctx.assign(z, false)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(true), Some(true)) | (_, Some(false), Some(false)) => {
                ctx.assign(x, true)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(false), Some(true)) | (_, Some(true), Some(false)) => {
                ctx.assign(x, false)?;
                Ok(prop::Status::Entailed)
            }

            (Some(true), _, Some(true)) | (Some(false), _, Some(false)) => {
                ctx.assign(y, true)?;
                Ok(prop::Status::Entailed)
            }

            (Some(false), _, Some(true)) | (Some(true), _, Some(false)) => {
                ctx.assign(y, false)?;
                Ok(prop::Status::Entailed)
            }

            (Some(_), None, None) => {
                if first_time {
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, Some(_), None) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, None, Some(_)) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Binary;
                Ok(prop::Status::Unsure)
            }

            (None, None, None) => {
                if first_time {
                    ctx.subscribe(x, var::ModTrigger::OnAssigned);
                    ctx.subscribe(y, var::ModTrigger::OnAssigned);
                    ctx.subscribe(z, var::ModTrigger::OnAssigned);
                }
                self.cost = prop::Cost::Ternary;
                Ok(prop::Status::Unsure)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn eq_assigned() {
        let mut vars = var::Store::<var::domains::BitSetDom>::new();
        let mut events = vec![];
        let mut ctx = prop::Context::new(prop::Token::new(0), &mut vars, &mut events);
        for &x in &[false, true] {
            for &y in &[false, true] {
                let mut prop = EqProp::new(x, y);
                if x == y {
                    assert!(prop.propagate(&mut ctx, true).unwrap().is_entailed())
                } else {
                    assert!(prop.propagate(&mut ctx, true).is_err())
                }
            }
        }
    }

    #[test]
    fn and_assigned() {
        let mut vars = var::Store::<var::domains::BitSetDom>::new();
        let mut events = vec![];
        let mut ctx = prop::Context::new(prop::Token::new(0), &mut vars, &mut events);
        for &x in &[false, true] {
            for &y in &[false, true] {
                for &z in &[false, true] {
                    let mut prop = AndProp::new(x, y, z);
                    if (x && y) == z {
                        assert!(prop.propagate(&mut ctx, true).unwrap().is_entailed())
                    } else {
                        assert!(prop.propagate(&mut ctx, true).is_err())
                    }
                }
            }
        }
    }

    #[test]
    fn iff_assigned() {
        let mut vars = var::Store::<var::domains::BitSetDom>::new();
        let mut events = vec![];
        let mut ctx = prop::Context::new(prop::Token::new(0), &mut vars, &mut events);
        for &x in &[false, true] {
            for &y in &[false, true] {
                for &z in &[false, true] {
                    let mut prop = IffProp::new(x, y, z);
                    if (x == y) == z {
                        assert!(prop.propagate(&mut ctx, true).unwrap().is_entailed())
                    } else {
                        assert!(prop.propagate(&mut ctx, true).is_err())
                    }
                }
            }
        }
    }
}
