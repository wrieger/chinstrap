use crate::{
    prop::{self, Propagate},
    types::{Error, Int, RelOp, Value},
    var::{self, Domain, Var, View},
};

pub fn make_prop<D, Vx, Vy, X, Y>(x: X, op: RelOp, y: Y) -> Box<dyn Propagate<Dom = D>>
where
    D: var::Domain,
    X: Into<Var<Vx>>,
    Y: Into<Var<Vy>>,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    let x = x.into();
    let y = y.into();
    match op {
        RelOp::Eq => Box::new(EqPropagator::new(x, y)),
        RelOp::Ne => Box::new(NeqPropagator::new(x, y)),
        RelOp::Lt => Box::new(LeqPropagator::new(x.offset(1), y)),
        RelOp::Le => Box::new(LeqPropagator::new(x, y)),
        RelOp::Gt => Box::new(LeqPropagator::new(y.offset(1), x)),
        RelOp::Ge => Box::new(LeqPropagator::new(y, x)),
    }
}

fn check_eq<D, T, Vx, Vy>(ctx: &prop::Context<'_, '_, D>, x: Var<Vx>, y: Var<Vy>) -> Option<bool>
where
    D: var::Domain,
    T: Value,
    Vx: var::View<Dom = D, Value = T>,
    Vy: var::View<Dom = D, Value = T>,
{
    match (ctx.value(x), ctx.value(y)) {
        (Some(x_val), Some(y_val)) => Some(x_val == y_val),
        _ => {
            if ctx.max(x) < ctx.min(y) || ctx.max(y) < ctx.min(x) {
                Some(false)
            } else {
                None
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct EqPropagator<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> EqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D>,
    Vy: View<Dom = D>,
{
    pub fn new<X, Y>(x: X, y: Y) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
        }
    }
}

impl<D, Vx, Vy> Propagate for EqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Binary
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, prop::Error> {
        let x = self.x;
        let y = self.y;

        match (ctx.value(x), ctx.value(y)) {
            (Some(x_val), _) => {
                ctx.assign(y, x_val)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(y_val)) => {
                ctx.assign(x, y_val)?;
                Ok(prop::Status::Entailed)
            }

            (None, None) => {
                // iteratively adjust the bounds until they're equal
                loop {
                    let mut mod_event = ctx.adjust_min(x, ctx.min(y))?;
                    mod_event |= ctx.adjust_min(y, ctx.min(x))?;
                    if mod_event == var::ModEvent::Unchanged {
                        break;
                    }
                }
                loop {
                    let mut mod_event = ctx.adjust_max(x, ctx.max(y))?;
                    mod_event |= ctx.adjust_max(y, ctx.max(x))?;
                    if mod_event == var::ModEvent::Unchanged {
                        break;
                    }
                }

                // after adjusting the bounds, check assignment again
                match (ctx.value(x), ctx.value(y)) {
                    (Some(x_val), _) => {
                        ctx.assign(y, x_val)?;
                        Ok(prop::Status::Entailed)
                    }

                    (_, Some(y_val)) => {
                        ctx.assign(x, y_val)?;
                        Ok(prop::Status::Entailed)
                    }

                    (None, None) => {
                        if first_time {
                            ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                            ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
                        }
                        Ok(prop::Status::Fixed)
                    }
                }
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct NeqPropagator<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> NeqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    pub fn new<X, Y>(x: X, y: Y) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
        }
    }
}

impl<D, Vx, Vy> Propagate for NeqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Binary
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, prop::Error> {
        let x = self.x;
        let y = self.y;

        match (ctx.value(x), ctx.value(y)) {
            (Some(x_val), _) => {
                ctx.remove(y, x_val)?;
                Ok(prop::Status::Entailed)
            }

            (_, Some(y_val)) => {
                ctx.remove(x, y_val)?;
                Ok(prop::Status::Entailed)
            }

            (None, None) => {
                if ctx.max(x) < ctx.min(y) || ctx.max(y) < ctx.min(x) {
                    if !first_time {
                        ctx.unsubscribe(x);
                        ctx.unsubscribe(y);
                    }
                    Ok(prop::Status::Entailed)
                } else {
                    if first_time {
                        ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                        ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
                    }
                    Ok(prop::Status::Fixed)
                }
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct LeqPropagator<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    x: Var<Vx>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> LeqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D>,
    Vy: View<Dom = D>,
{
    pub fn new<X, Y>(x: X, y: Y) -> Self
    where
        X: Into<Var<Vx>>,
        Y: Into<Var<Vy>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
        }
    }
}

impl<D, Vx, Vy> Propagate for LeqPropagator<Vx, Vy>
where
    D: Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, Error> {
        let x = self.x;
        let y = self.y;

        ctx.adjust_max(x, ctx.max(y))?;
        ctx.adjust_min(y, ctx.min(x))?;

        if ctx.max(x) <= ctx.min(y) {
            if !first_time {
                ctx.unsubscribe(x);
                ctx.unsubscribe(y);
            }
            Ok(prop::Status::Entailed)
        } else {
            if first_time {
                ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
            }
            Ok(prop::Status::Fixed)
        }
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Binary
    }
}

// TODO rewrite tests
// #[cfg(test)]
// mod tests {
//     use super::*;

//     use crate::{prop::Token, var};

//     #[test]
//     fn eq_assigned() {
//         let mut doms = var::Store::new();
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         for x in -10..=10 {
//             for y in -10..=10 {
//                 let mut prop = EqPropagator::new(x, y);
//                 let res = prop.propagate(&mut ctx, true);
//                 if x == y {
//                     assert_eq!(res, Ok(prop::Status::Entailed));
//                 } else {
//                     assert!(res.is_err());
//                 }
//             }
//         }
//     }

//     #[test]
//     fn eq_unassigned_cascade() {
//         let mut doms = var::Store::new();
//         let x = doms.add(var::Domain::btree_from_bounds(0, 9));
//         let y = doms.add(var::Domain::btree_from_bounds(0, 9));
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         let mut prop = EqPropagator::new(x.scale(2), y.scale(2).offset(1));
//         assert!(prop.propagate(&mut ctx, true).is_err());
//     }

//     #[test]
//     fn neq_assigned() {
//         let mut doms = var::Store::new();
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         for x in -10..=10 {
//             for y in -10..=10 {
//                 let mut prop = NeqPropagator::new(x, y);
//                 let res = prop.propagate(&mut ctx, true);
//                 if x != y {
//                     assert_eq!(res, Ok(prop::Status::Entailed));
//                 } else {
//                     assert!(res.is_err());
//                 }
//             }
//         }
//     }

//     #[test]
//     fn leq_assigned() {
//         let mut doms = var::Store::new();
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         for x in -10..=10 {
//             for y in -10..=10 {
//                 let mut prop = LeqPropagator::new(x, y);
//                 let res = prop.propagate(&mut ctx, true);
//                 if x <= y {
//                     assert_eq!(res, Ok(prop::Status::Entailed));
//                 } else {
//                     assert!(res.is_err());
//                 }
//             }
//         }
//     }

//     #[test]
//     fn leq_overlap() {
//         let mut doms = var::Store::new();
//         let x = doms.add(var::Domain::btree_from_bounds(10, 30));
//         let y = doms.add(var::Domain::btree_from_bounds(0, 20));
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         let mut prop = LeqPropagator::new(x, y);
//         assert_eq!(prop.propagate(&mut ctx, true), Ok(prop::Status::Fixed));
//         assert_eq!(doms.min(x), Ok(10));
//         assert_eq!(doms.max(x), Ok(20));
//         assert_eq!(doms.min(y), Ok(10));
//         assert_eq!(doms.max(y), Ok(20));
//     }

//     #[test]
//     fn leq_entailed() {
//         let mut doms = var::Store::new();
//         let x = doms.add(var::Domain::btree_from_bounds(0, 10));
//         let y = doms.add(var::Domain::btree_from_bounds(20, 30));
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         let mut prop = LeqPropagator::new(x, y);
//         let res = prop.propagate(&mut ctx, true);
//         assert_eq!(res, Ok(prop::Status::Entailed));
//     }

//     #[test]
//     fn leq_unentailed() {
//         let mut doms = var::Store::new();
//         let x = doms.add(var::Domain::btree_from_bounds(20, 30));
//         let y = doms.add(var::Domain::btree_from_bounds(0, 10));
//         let mut ctx = prop::Context::new(Token::new(0), &mut doms);

//         let mut prop = LeqPropagator::new(x, y);
//         let res = prop.propagate(&mut ctx, true);
//         assert!(res.is_err());
//     }
// }
