use crate::{prop, types, var};

/// Propagator for `|x| == y`.
#[derive(Clone, Debug)]
pub struct AbsProp<Vx, Vy>
where
    Vx: var::View,
    Vy: var::View,
{
    x: var::Var<Vx>,
    y: var::Var<Vy>,
}

impl<D, Vx, Vy> AbsProp<Vx, Vy>
where
    D: var::Domain,
    Vx: var::View<Dom = D, Value = types::Int>,
    Vy: var::View<Dom = D, Value = types::Int>,
{
    pub fn new<X, Y>(x: X, y: Y) -> Self
    where
        X: Into<var::Var<Vx>>,
        Y: Into<var::Var<Vy>>,
    {
        Self {
            x: x.into(),
            y: y.into(),
        }
    }
}

impl<D, Vx, Vy> prop::Propagate for AbsProp<Vx, Vy>
where
    D: var::Domain,
    Vx: var::View<Dom = D, Value = types::Int> + 'static,
    Vy: var::View<Dom = D, Value = types::Int> + 'static,
{
    type Dom = D;

    fn boxed_clone(&self) -> Box<dyn prop::Propagate<Dom = D>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> prop::Cost {
        prop::Cost::Binary
    }

    fn propagate(
        &mut self,
        ctx: &mut prop::Context<'_, '_, D>,
        first_time: bool,
    ) -> Result<prop::Status<D>, types::Error> {
        let x = self.x;
        let y = self.y;

        ctx.adjust_min(y, 0)?;

        if ctx.min(x) >= 0 {
            Ok(prop::Status::Rewrite(prop::rel::make_prop(
                x,
                types::RelOp::Eq,
                y,
            )))
        } else if ctx.max(x) <= 0 {
            Ok(prop::Status::Rewrite(prop::rel::make_prop(
                x.minus(),
                types::RelOp::Eq,
                y,
            )))
        } else {
            if first_time {
                ctx.subscribe(x, var::ModTrigger::OnBoundsChanged);
                ctx.subscribe(y, var::ModTrigger::OnBoundsChanged);
            }
            ctx.adjust_max(y, Ord::max(ctx.max(x), -ctx.min(x)))?;
            ctx.adjust_max(x, ctx.max(y))?;
            ctx.adjust_min(x, -ctx.max(y))?;
            if ctx.min(y) > 0 {
                if ctx.min(x) > -ctx.min(y) {
                    ctx.adjust_min(x, ctx.min(y))?;
                } else if ctx.max(x) < ctx.min(y) {
                    ctx.adjust_max(x, -ctx.min(y))?;
                }
            }
            Ok(prop::Status::Unsure)
        }
    }
}
