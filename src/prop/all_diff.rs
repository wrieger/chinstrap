use crate::{
    prop::{Context, Cost, Error, Propagate, Status},
    types::Int,
    var::{self, ModTrigger, Var, View},
};

#[derive(Clone, Debug)]
pub struct AllDiffProp<V>
where
    V: View,
{
    xs: Vec<Var<V>>,
}

impl<V> AllDiffProp<V>
where
    V: View,
{
    pub fn new<X>(xs: Vec<X>) -> Self
    where
        X: Into<Var<V>>,
    {
        Self {
            xs: xs.into_iter().map(|x| x.into()).collect(),
        }
    }
}

impl<V> Propagate for AllDiffProp<V>
where
    V: View<Value = Int> + 'static,
{
    type Dom = V::Dom;

    fn boxed_clone(&self) -> Box<dyn Propagate<Dom = V::Dom>> {
        Box::new(self.clone())
    }

    fn cost(&self) -> Cost {
        Cost::Quadratic
    }

    fn propagate(
        &mut self,
        ctx: &mut Context<'_, '_, V::Dom>,
        first_time: bool,
    ) -> Result<Status<V::Dom>, Error> {
        if first_time {
            for &x in &self.xs {
                ctx.subscribe(x, ModTrigger::OnAssigned);
            }
        }

        loop {
            let mut mod_event = var::ModEvent::Unchanged;
            for i in (0..self.xs.len()).rev() {
                let x = self.xs[i];
                if let Some(val) = ctx.value(x) {
                    for (_, &y) in self.xs.iter().enumerate().filter(|&(j, _)| i != j) {
                        mod_event |= ctx.remove(y, val)?;
                    }
                    self.xs.swap_remove(i);
                }
            }

            if mod_event != var::ModEvent::Assigned {
                break;
            }
        }

        if self.xs.len() <= 1 {
            Ok(Status::Entailed)
        } else {
            let mut xs = self.xs.clone();
            // let mut minus_xs = self.xs.clone().into_iter().map(|x| x.minus()).collect();
            filter_n3(ctx, &mut xs)?;
            // filter_n3(ctx, &mut minus_xs)?;
            Ok(Status::Unsure)
        }
    }
}

fn filter_n3<V>(ctx: &mut Context<'_, '_, V::Dom>, xs: &mut Vec<Var<V>>) -> Result<(), Error>
where
    V: View<Value = Int>,
{
    xs.sort_by_key(|&x| ctx.max(x));
    let mins = xs.iter().map(|&x| ctx.min(x)).collect::<Vec<_>>();
    let maxs = xs.iter().map(|&x| ctx.max(x)).collect::<Vec<_>>();

    let mut us = mins.clone();
    for i in 0..xs.len() {
        insert(ctx, &xs, &mins, &maxs, &mut us, i)?;
    }
    Ok(())
}

fn insert<V>(
    ctx: &mut Context<'_, '_, V::Dom>,
    xs: &Vec<Var<V>>,
    mins: &Vec<Int>,
    maxs: &Vec<Int>,
    us: &mut Vec<Int>,
    i: usize,
) -> Result<(), Error>
where
    V: View<Value = Int>,
{
    for j in 0..i {
        if mins[j] < mins[i] {
            us[j] += 1;
            if us[j] > maxs[i] {
                return Err(Error::PropUnentailed);
            } else if us[j] == maxs[i] {
                incr_min(ctx, &xs, mins[j], maxs[i], i)?;
            }
        } else {
            us[i] += 1;
        }
    }
    if us[i] > maxs[i] {
        return Err(Error::PropUnentailed);
    } else if us[i] == maxs[i] {
        incr_min(ctx, &xs, mins[i], maxs[i], i)?;
    }
    Ok(())
}

fn incr_min<V>(
    ctx: &mut Context<'_, '_, V::Dom>,
    xs: &Vec<Var<V>>,
    a: Int,
    b: Int,
    i: usize,
) -> Result<(), Error>
where
    V: View<Value = Int>,
{
    for j in (i + 1)..xs.len() {
        if ctx.min(xs[j]) >= a {
            ctx.adjust_min(xs[j], b + 1)?;
        }
    }
    Ok(())
}
