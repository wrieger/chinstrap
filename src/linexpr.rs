use std::{
    collections::BTreeMap,
    ops::{Add, Mul, Neg, Sub},
};

use crate::{
    types::Int,
    var::{self, IntVar},
};

pub enum TryIntoVarResult<D>
where
    D: var::Domain,
{
    Const(var::ConstIntVar<D>),
    PosExpr(var::PosAffineIntVar<D>),
    NegExpr(var::NegAffineIntVar<D>),
    CannotIntoVar(LinExpr<D>),
}

#[derive(Clone, Debug)]
pub struct LinExpr<D>
where
    D: var::Domain,
{
    vars: BTreeMap<IntVar<D>, Int>,
    c: Int,
}

impl<D> LinExpr<D>
where
    D: var::Domain,
{
    pub fn constant(&self) -> Int {
        self.c
    }

    pub fn vars<'a>(&'a self) -> impl Iterator<Item = (IntVar<D>, Int)> + 'a {
        self.vars.iter().map(|(&x, &c)| (x, c))
    }

    pub fn try_into_var(mut self) -> TryIntoVarResult<D> {
        self.normalize();
        if self.vars.len() == 0 {
            TryIntoVarResult::Const(var::const_int_var(self.c))
        } else if self.vars.len() == 1 {
            let (x, coeff) = self.vars().next().unwrap();
            if coeff > 0 {
                TryIntoVarResult::PosExpr(x.scale(coeff).offset(self.c))
            } else if coeff < 0 {
                TryIntoVarResult::NegExpr(x.scale(coeff).minus().offset(self.c))
            } else {
                unreachable!()
            }
        } else {
            TryIntoVarResult::CannotIntoVar(self)
        }
    }

    fn normalize(&mut self) {
        self.vars = self
            .vars
            .iter()
            .filter(|(_, &coeff)| coeff != 0)
            .map(|(x, coeff)| (*x, *coeff))
            .collect();
    }
}

impl<D> From<Int> for LinExpr<D>
where
    D: var::Domain,
{
    fn from(c: Int) -> Self {
        Self {
            vars: BTreeMap::new(),
            c,
        }
    }
}

impl<D> From<IntVar<D>> for LinExpr<D>
where
    D: var::Domain,
{
    fn from(x: IntVar<D>) -> Self {
        Self {
            vars: vec![(x, 1)].into_iter().collect(),
            c: 0,
        }
    }
}

impl<D> Add<LinExpr<D>> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: Self) -> LinExpr<D> {
        let mut merged = self.vars;
        for (x, c) in rhs.vars {
            let entry = merged.entry(x).or_insert(0);
            *entry = *entry + c;
        }
        let mut sum = Self {
            vars: merged,
            c: self.c + rhs.c,
        };
        sum.normalize();
        sum
    }
}

impl<D> Add<IntVar<D>> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: IntVar<D>) -> LinExpr<D> {
        LinExpr::from(self) + LinExpr::from(rhs)
    }
}

impl<D> Add<IntVar<D>> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: IntVar<D>) -> LinExpr<D> {
        self + LinExpr::from(rhs)
    }
}

impl<D> Add<LinExpr<D>> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: LinExpr<D>) -> LinExpr<D> {
        LinExpr::from(self) + rhs
    }
}

impl<D> Add<Int> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: Int) -> LinExpr<D> {
        self + LinExpr::from(rhs)
    }
}

impl<D> Add<LinExpr<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: LinExpr<D>) -> LinExpr<D> {
        LinExpr::from(self) + rhs
    }
}

impl<D> Add<Int> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: Int) -> LinExpr<D> {
        self + LinExpr::from(rhs)
    }
}

impl<D> Add<IntVar<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn add(self, rhs: IntVar<D>) -> LinExpr<D> {
        LinExpr::from(self) + rhs
    }
}

impl<D> Sub<LinExpr<D>> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: Self) -> LinExpr<D> {
        let mut merged = self.vars;
        for (x, coeff) in rhs.vars {
            let entry = merged.entry(x).or_insert(0);
            *entry = *entry - coeff;
        }
        let mut diff = Self {
            vars: merged,
            c: self.c - rhs.c,
        };
        diff.normalize();
        diff
    }
}

impl<D> Sub<IntVar<D>> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: IntVar<D>) -> LinExpr<D> {
        LinExpr::from(self) - LinExpr::from(rhs)
    }
}

impl<D> Sub<IntVar<D>> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: IntVar<D>) -> LinExpr<D> {
        self - LinExpr::from(rhs)
    }
}

impl<D> Sub<LinExpr<D>> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: LinExpr<D>) -> LinExpr<D> {
        LinExpr::from(self) - rhs
    }
}

impl<D> Sub<Int> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: Int) -> LinExpr<D> {
        self - LinExpr::from(rhs)
    }
}

impl<D> Sub<LinExpr<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: LinExpr<D>) -> LinExpr<D> {
        LinExpr::from(self) - rhs
    }
}

impl<D> Sub<Int> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: Int) -> LinExpr<D> {
        self - LinExpr::from(rhs)
    }
}

impl<D> Sub<IntVar<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn sub(self, rhs: IntVar<D>) -> LinExpr<D> {
        LinExpr::from(self) - rhs
    }
}

impl<D> Mul<Int> for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn mul(self, rhs: Int) -> LinExpr<D> {
        let mut prod = LinExpr {
            vars: self
                .vars
                .into_iter()
                .map(|(x, coeff)| (x, coeff * rhs))
                .collect(),
            c: self.c * rhs,
        };
        prod.normalize();
        prod
    }
}

impl<D> Mul<LinExpr<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn mul(self, rhs: LinExpr<D>) -> LinExpr<D> {
        rhs * self
    }
}

impl<D> Mul<Int> for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn mul(self, rhs: Int) -> LinExpr<D> {
        LinExpr::from(self) * rhs
    }
}

impl<D> Mul<IntVar<D>> for Int
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn mul(self, rhs: IntVar<D>) -> LinExpr<D> {
        rhs * self
    }
}

impl<D> Neg for LinExpr<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn neg(self) -> LinExpr<D> {
        -1 * self
    }
}

impl<D> Neg for IntVar<D>
where
    D: var::Domain,
{
    type Output = LinExpr<D>;

    fn neg(self) -> LinExpr<D> {
        -1 * self
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn does_it_work() {
//         let x = IntVar<D> { index: 0 };
//         let y = IntVar<D> { index: 1 };
//         assert_eq!(-y - y + 2 * (x * 3 + y - 2) - 1, 6 * x - 5);
//     }
// }
