use std::{cell::RefCell, rc::Rc};

use crate::{
    linexpr::LinExpr,
    prop,
    search::State,
    types::{Int, RelOp, Value},
    var::{self, Var, View},
};

pub struct ArithNeBuilder<D>
where
    D: var::Domain,
{
    lhs: LinExpr<D>,
    rhs: LinExpr<D>,
    state: Rc<RefCell<State<D>>>,
}

impl<D> ArithNeBuilder<D>
where
    D: var::Domain + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, lhs: LinExpr<D>, rhs: LinExpr<D>) -> Self {
        Self { state, lhs, rhs }
    }

    pub fn post(self) {
        let expr = self.lhs - self.rhs;
        let c = expr.constant();
        let mut pos_vars = vec![];
        let mut neg_vars = vec![];
        for (x, coeff) in expr.vars() {
            if coeff > 0 {
                pos_vars.push(x.scale(coeff));
            } else if coeff < 0 {
                neg_vars.push(x.scale(-coeff));
            } else {
                unreachable!()
            }
        }
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumNeqProp::<_, _>::new(
                pos_vars, neg_vars, -c,
            )));
    }
}

pub struct ArithLtBuilder<D>
where
    D: var::Domain,
{
    lhs: LinExpr<D>,
    rhs: LinExpr<D>,
    state: Rc<RefCell<State<D>>>,
}

impl<D> ArithLtBuilder<D>
where
    D: var::Domain + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, lhs: LinExpr<D>, rhs: LinExpr<D>) -> Self {
        Self { state, lhs, rhs }
    }

    pub fn post(self) {
        let expr = self.lhs - self.rhs;
        let c = expr.constant();
        let mut pos_vars = vec![];
        let mut neg_vars = vec![];
        for (x, coeff) in expr.vars() {
            if coeff > 0 {
                pos_vars.push(x.scale(coeff));
            } else if coeff < 0 {
                neg_vars.push(x.scale(-coeff));
            } else {
                unreachable!()
            }
        }
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumLeqProp::<_, _>::new(
                pos_vars, neg_vars, -c - 1,
            )));
    }
}

pub struct ArithBuilder<D>
where
    D: var::Domain,
{
    lhs: LinExpr<D>,
    rhs: LinExpr<D>,
    state: Rc<RefCell<State<D>>>,
}

impl<D> ArithBuilder<D>
where
    D: var::Domain + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, lhs: LinExpr<D>, rhs: LinExpr<D>) -> Self {
        Self { state, lhs, rhs }
    }

    pub fn post(self) {
        let expr = self.lhs - self.rhs;
        let c = expr.constant();
        let mut pos_vars = vec![];
        let mut neg_vars = vec![];
        for (x, coeff) in expr.vars() {
            if coeff > 0 {
                pos_vars.push(x.scale(coeff));
            } else if coeff < 0 {
                neg_vars.push(x.scale(-coeff));
            } else {
                unreachable!()
            }
        }
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumProp::<_, _>::new(
                pos_vars, neg_vars, -c,
            )));
    }
}

pub struct RelBuilder<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    x: Var<Vx>,
    op: RelOp,
    y: Var<Vy>,
}

impl<D, Vx, Vy> RelBuilder<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = i32> + 'static,
    Vy: View<Dom = D, Value = i32> + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, x: Var<Vx>, op: RelOp, y: Var<Vy>) -> Self {
        Self { state, x, op, y }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(prop::rel::make_prop(self.x, self.op, self.y));
    }
}

pub struct AbsBuilder<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    x: Var<Vx>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> AbsBuilder<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = i32> + 'static,
    Vy: View<Dom = D, Value = i32> + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, x: Var<Vx>, y: Var<Vy>) -> Self {
        Self { state, x, y }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::math::AbsProp::new(self.x, self.y)));
    }
}

#[must_use]
pub struct AllDiffBuilder<V>
where
    V: View,
{
    state: Rc<RefCell<State<V::Dom>>>,
    vars: Vec<Var<V>>,
    use_decomp: bool,
}

impl<V> AllDiffBuilder<V>
where
    V: View<Value = Int> + 'static,
{
    pub fn new(state: Rc<RefCell<State<V::Dom>>>, vars: Vec<Var<V>>, use_decomp: bool) -> Self {
        Self {
            state,
            vars,
            use_decomp,
        }
    }

    pub fn use_decomp(&mut self, use_decomp: bool) -> &mut Self {
        self.use_decomp = use_decomp;
        self
    }

    pub fn post(self) {
        if self.use_decomp {
            unimplemented!()
        } else {
            self.state
                .borrow_mut()
                .add_prop(Box::new(prop::all_diff::AllDiffProp::new(self.vars)));
        }
    }
}

#[must_use]
pub struct SumEqBuilder<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    x: Vec<Var<Vx>>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> SumEqBuilder<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, x: Vec<Var<Vx>>, y: Var<Vy>) -> Self {
        Self { state, x, y }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumProp::new(self.x, vec![self.y], 0)));
    }
}

#[must_use]
pub struct SumLeqBuilder<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    x: Vec<Var<Vx>>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> SumLeqBuilder<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, x: Vec<Var<Vx>>, y: Var<Vy>) -> Self {
        Self { state, x, y }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumLeqProp::new(
                self.x,
                vec![self.y],
                0,
            )));
    }
}

#[must_use]
pub struct SumNeqBuilder<Vx, Vy>
where
    Vx: View,
    Vy: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    x: Vec<Var<Vx>>,
    y: Var<Vy>,
}

impl<D, Vx, Vy> SumNeqBuilder<Vx, Vy>
where
    D: var::Domain,
    Vx: View<Dom = D, Value = Int> + 'static,
    Vy: View<Dom = D, Value = Int> + 'static,
{
    pub fn new(state: Rc<RefCell<State<D>>>, x: Vec<Var<Vx>>, y: Var<Vy>) -> Self {
        Self { state, x, y }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::sum::SumNeqProp::new(
                self.x,
                vec![self.y],
                0,
            )));
    }
}

#[must_use]
pub struct CountBuilder<Vx, Vy, Vz>
where
    Vx: View,
    Vy: View,
    Vz: View,
{
    state: Rc<RefCell<State<Vx::Dom>>>,
    vars: Vec<Var<Vx>>,
    value: Var<Vy>,
    count: Var<Vz>,
}

impl<D, T, Vx, Vy, Vz> CountBuilder<Vx, Vy, Vz>
where
    D: var::Domain,
    T: Value,
    Vx: View<Dom = D, Value = T> + 'static,
    Vy: View<Dom = D, Value = T> + 'static,
    Vz: View<Dom = D, Value = Int> + 'static,
{
    pub fn new(
        state: Rc<RefCell<State<D>>>,
        vars: Vec<Var<Vx>>,
        value: Var<Vy>,
        count: Var<Vz>,
    ) -> Self {
        Self {
            state,
            vars,
            value,
            count,
        }
    }

    pub fn post(self) {
        self.state
            .borrow_mut()
            .add_prop(Box::new(prop::count::CountEqProp::new(
                self.vars, self.value, self.count,
            )));
    }
}
