use std::ops::Index;
use std::iter::FromIterator;

use crate::model::Array;

pub struct Matrix<T> {
    n_rows: usize,
    n_cols: usize,
    values: Vec<T>,
}

impl<T> Matrix<T> {
    fn get(&self, r: usize, c: usize) -> &T {
        assert!(r < self.n_rows);
        assert!(c < self.n_cols);
        &self.values[r * self.n_cols + c]
    }

    pub fn row<'a>(&'a self, r: usize) -> impl Iterator<Item = &'a T> + 'a {
        assert!(r < self.n_rows);
        (0..self.n_cols).map(move |c| self.get(r, c))
    }

    pub fn col<'a>(&'a self, c: usize) -> impl Iterator<Item = &'a T> + 'a {
        assert!(c < self.n_cols);
        (0..self.n_rows).map(move |r| self.get(r, c))
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            inner: self.values.iter(),
        }
    }

    pub fn sub_matrix<'a>(
        &'a self,
        r: usize,
        c: usize,
        n_rows: usize,
        n_cols: usize,
    ) -> impl Iterator<Item = &'a T> + 'a {
        assert!(r + n_rows <= self.n_rows);
        assert!(c + n_cols <= self.n_cols);
        assert!(n_rows > 0);
        assert!(n_cols > 0);
        (0..(n_rows * n_cols)).map(move |i| {
            let r_offset = i / n_rows;
            let c_offset = i % n_rows;
            self.get(r + r_offset, c + c_offset)
        })
    }
}

impl<T> IntoIterator for Matrix<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        IntoIter {
            inner: self.values.into_iter(),
        }
    }
}

impl<'a, T> IntoIterator for &'a Matrix<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        Iter {
            inner: self.values.iter(),
        }
    }
}

pub struct IntoIter<T> {
    inner: std::vec::IntoIter<T>,
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.inner.next()
    }
}

pub struct Iter<'a, T>
where
    T: 'a,
{
    inner: std::slice::Iter<'a, T>,
}

impl<'a, T> Iterator for Iter<'a, T>
where
    T: 'a,
{
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        self.inner.next()
    }
}

impl<T> FromIterator<Array<T>> for Matrix<T>
where
    T: Clone,
{
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = Array<T>>,
    {
        let rows = iter.into_iter().collect::<Vec<_>>();
        let n_rows = rows.len();
        let n_cols = rows[0].len();
        Self {
            values: rows
                .into_iter()
                .map(|array| array.into_iter())
                .flatten()
                .collect(),
            n_rows,
            n_cols,
        }
    }
}

impl<T> Index<(usize, usize)> for Matrix<T>
where
    T: Clone,
{
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &T {
        self.get(index.0, index.1)
    }
}