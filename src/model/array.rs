use std::{iter::FromIterator, ops::Index};

pub struct Array<T> {
    values: Vec<T>,
}

impl<T> Array<T> {
    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            inner: self.values.iter(),
        }
    }
}

impl<T> FromIterator<T> for Array<T> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = T>,
    {
        Self {
            values: iter.into_iter().collect(),
        }
    }
}

impl<T> Index<usize> for Array<T> {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        assert!(index < self.values.len());
        &self.values[index]
    }
}

impl<T> IntoIterator for Array<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        IntoIter {
            inner: self.values.into_iter(),
        }
    }
}

impl<'a, T> IntoIterator for &'a Array<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        Iter {
            inner: self.values.iter(),
        }
    }
}

pub struct IntoIter<T> {
    inner: std::vec::IntoIter<T>,
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.inner.next()
    }
}

pub struct Iter<'a, T>
where
    T: 'a,
{
    inner: std::slice::Iter<'a, T>,
}

impl<'a, T> Iterator for Iter<'a, T>
where
    T: 'a,
{
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        self.inner.next()
    }
}
