use std::fmt::Debug;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum RelOp {
    Eq,
    Ne,
    Ge,
    Gt,
    Le,
    Lt,
}

pub type Int = i32;

/// A value that a view can produce.
pub trait Value: Copy + Ord + Debug {}

impl Value for Int {}
impl Value for bool {}

/// Error.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Error {
    DomEmpty,
    PropUnentailed,
}
